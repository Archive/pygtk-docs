XSLFILES = common.xsl html.xsl tut-html-style.xsl \
	pdf-style.xsl pdf.xsl devhelp.xsl

CSSFILES = style.css

TUTORIALXMLFILES = \
tut/pygtk2-tut.xml \
tut/Adjustments.xml \
tut/AdvancedEventAndSignalHandling.xml \
tut/ButtonWidget.xml \
tut/CellRenderers.xml \
tut/CodeExamples.xml \
tut/ComboBoxAndComboBoxEntry.xml \
tut/ContainerWidgets.xml \
tut/Contributing.xml \
tut/Credits.xml \
tut/Copyright.xml \
tut/DragAndDrop.xml \
tut/DrawingArea.xml \
tut/EntryCompletion.xml \
tut/ExpanderWidget.xml \
tut/FileChooser.xml \
tut/GenericCellRenderer.xml \
tut/GenericTreeModel.xml \
tut/GettingStarted.xml \
tut/GtkRcFiles.xml \
tut/GtkSignals.xml \
tut/Introduction.xml \
tut/ManagingSelections.xml \
tut/MenuWidget.xml \
tut/MiscellaneousWidgets.xml \
tut/MovingOn.xml \
tut/NewInPyGTK24.xml \
tut/NewWidgetsAndObjects.xml \
tut/PackingWidgets.xml \
tut/pygtk2-tut.xml \
tut/RangeWidgets.xml \
tut/Scribble.xml \
tut/SettingWidgetAttributes.xml \
tut/TextViewWidget.xml \
tut/TimeoutsIOAndIdleFunctions.xml \
tut/TipsForWritingPygtkApplications.xml \
tut/TreeViewWidget.xml \
tut/UndocumentedWidgets.xml \
tut/WidgetOverview.xml \
tut/ChangeLog.xml

PICSTEMS= \
singlewidget \
twowidget

TUTORIALLINKS = \
pygtk2tutorial/images \
pygtk2tutorial/examples \
pygtk2tutorial/figures

ESOUTPUTDIR=pygtk2tutorial-es

ESTUTORIALXMLFILES = \
tut-es/pygtk2-tut.xml \
tut-es/Adjustments.xml \
tut-es/AdvancedEventAndSignalHandling.xml \
tut-es/ButtonWidget.xml \
tut-es/CellRenderers.xml \
tut-es/CodeExamples.xml \
tut-es/ComboBoxAndComboBoxEntry.xml \
tut-es/ContainerWidgets.xml \
tut-es/Contributing.xml \
tut-es/Credits.xml \
tut-es/Copyright.xml \
tut-es/DragAndDrop.xml \
tut-es/DrawingArea.xml \
tut-es/EntryCompletion.xml \
tut-es/ExpanderWidget.xml \
tut-es/FileChooser.xml \
tut-es/GenericCellRenderer.xml \
tut-es/GenericTreeModel.xml \
tut-es/GettingStarted.xml \
tut-es/GtkRcFiles.xml \
tut-es/GtkSignals.xml \
tut-es/Introduction.xml \
tut-es/ManagingSelections.xml \
tut-es/MenuWidget.xml \
tut-es/MiscellaneousWidgets.xml \
tut-es/MovingOn.xml \
tut-es/NewInPyGTK24.xml \
tut-es/NewWidgetsAndObjects.xml \
tut-es/PackingWidgets.xml \
tut-es/pygtk2-tut.xml \
tut-es/RangeWidgets.xml \
tut-es/Scribble.xml \
tut-es/SettingWidgetAttributes.xml \
tut-es/TextViewWidget.xml \
tut-es/TimeoutsIOAndIdleFunctions.xml \
tut-es/TipsForWritingPygtkApplications.xml \
tut-es/TreeModel.xml \
tut-es/TreeViewWidget.xml \
tut-es/UndocumentedWidgets.xml \
tut-es/WidgetOverview.xml \
tut-es/ChangeLog.xml

ESTUTORIALLINKS = \
${ESOUTPUTDIR}/images \
${ESOUTPUTDIR}/examples \
${ESOUTPUTDIR}/figures

tut-html: pygtk2tutorial ${TUTORIALLINKS} ${TUTORIALXMLFILES} ${XSLFILES} \
		${CSSFILES}
	cp ${CSSFILES} pygtk2tutorial
	xsltproc --nonet --xinclude -o pygtk2tutorial/ \
                 --stringparam gtkdoc.bookname pygtk2tutorial \
		tut-html-style.xsl tut/pygtk2-tut.xml

# symlinking isn't enough, dblatex wants the real ones
# for images it's ok to symlink them here
tut-pdf: pygtk2tutorial ${TUTORIALLINKS} ${TUTORIALXMLFILES} ${XSLFILES}
	cp tut/*.xml pygtk2tutorial/ 
	dblatex -V -t pdf -T db2latex -p pdf-dblatex.xsl -o pygtk2-tut.pdf \
		pygtk2tutorial/pygtk2-tut.xml

tut/ChangeLog.xml: tut/ChangeLog
	(echo '<literallayout>'; \
		sed \
		-e 's/&/\&amp;/g' \
		-e 's/</\&lt;/g' \
		-e 's/>/\&gt;/g' \
		-e 's/^	\* \([^ ]*\) \(.*\)$$/	* <filename>\1<\/filename> \2/' \
		-e 's/	/        /' \
		-e '/^20[0-9][0-9]-[0-9][0-9]-.*$$/s/^\(.*\)$$/<emphasis role="bold">\1<\/emphasis>/' \
		-e 's/(\([^)]*\))/(<emphasis>\1<\/emphasis>)/g' \
		tut/ChangeLog; \
		echo '</literallayout>') \
		> tut/ChangeLog.xml

tut-srcdist:
	tar zcf pygtk2-tut.docbook.tgz ${TUTORIALXMLFILES} ${XSLFILES} \
		$(PICFILES) ${CSSFILES} tut/ChangeLog Makefile examples \
		figures images

tut-dist:
	tar zhcf pygtk2tutorial.tgz pygtk2tutorial

pygtk2tutorial:
	-mkdir pygtk2tutorial

pygtk2tutorial/images:
	-mkdir pygtk2tutorial/images
	-(cd pygtk2tutorial/images; ln -s ../../images/* .)

pygtk2tutorial/examples:
	-mkdir pygtk2tutorial/examples
	-(cd pygtk2tutorial/examples; ln -s ../../examples/* .)

pygtk2tutorial/figures:
	-mkdir pygtk2tutorial/figures
	-(cd pygtk2tutorial/figures; ln -s ../../figures/* .)
	for x in $(PICSTEMS); do pic2graph <tut/$$x.pic >pygtk2tutorial/figures/$$x.png; done

tut-es-html: ${ESOUTPUTDIR} ${ESTUTORIALLINKS} ${ESTUTORIALXMLFILES} \
		${XSLFILES} ${CSSFILES}
	cp ${CSSFILES} ${ESOUTPUTDIR}
	xsltproc --nonet --xinclude -o ${ESOUTPUTDIR}/ \
		--stringparam gtkdoc.bookname  pygtk2tutorial \
		tut-html-style.xsl tut-es/pygtk2-tut.xml

tut-es-pdf: ${ESOUTPUTDIR} ${ESTUTORIALLINKS} ${ESTUTORIALXMLFILES} ${XSLFILES}
	cp tut-es/*.xml ${ESOUTPUTDIR}/ 
	dblatex -t pdf -T db2latex -p pdf-dblatex.xsl -o pygtk2-tut-es.pdf \
		${ESOUTPUTDIR}/pygtk2-tut.xml

tut-es/ChangeLog.xml: tut-es/ChangeLog
	(echo '<literallayout>'; \
		sed \
		-e 's/&/\&amp;/g' \
		-e 's/</\&lt;/g' \
		-e 's/>/\&gt;/g' \
		-e 's/^	\* \([^ ]*\) \(.*\)$$/	* <filename>\1<\/filename> \2/' \
		-e 's/	/        /' \
		-e '/^20[0-9][0-9]-[0-9][0-9]-.*$$/s/^\(.*\)$$/<emphasis role="bold">\1<\/emphasis>/' \
		-e 's/(\([^)]*\))/(<emphasis>\1<\/emphasis>)/g' \
		tut-es/ChangeLog; \
		echo '</literallayout>') \
		> tut-es/ChangeLog.xml

tut-es-srcdist:
	tar zcf pygtk2-tut-es.docbook.tgz ${ESTUTORIALXMLFILES} ${XSLFILES} \
		${CSSFILES} tut-es/ChangeLog Makefile examples figures images

tut-es-dist: tut-html tut-pdf
	cp pygtk2-tut-es.pdf ${ESOUTPUTDIR}/pygtk2tutorial-es.pdf
	tar zhcf pygtk2tutorial-es.tgz ${ESOUTPUTDIR}

${ESOUTPUTDIR}:
	-mkdir ${ESOUTPUTDIR}

${ESOUTPUTDIR}/images:
	-mkdir ${ESOUTPUTDIR}/images
	-(cd ${ESOUTPUTDIR}/images; ln -s ../../images/* .)

${ESOUTPUTDIR}/examples:
	-mkdir ${ESOUTPUTDIR}/examples
	-(cd ${ESOUTPUTDIR}/examples; ln -s ../../examples/* .)

${ESOUTPUTDIR}/figures:
	-mkdir ${ESOUTPUTDIR}/figures
	-(cd ${ESOUTPUTDIR}/figures; ln -s ../../figures/* .)

clean:
	-rm -rf pygtk2tutorial ${ESOUTPUTDIR}
	-rm -rf pygtk2tutorial.tgz pygtk2tutorial-es.tgz
	-rm -rf pygtk2-tut.pdf 
	-rm -rf	pygtk2-tut-es.pdf 
	-rm -f tut/ChangeLog.xml
