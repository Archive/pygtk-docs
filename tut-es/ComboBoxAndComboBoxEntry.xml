<?xml version="1.0" encoding="iso-8859-1" standalone="no"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
    "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">

<sect1 id="sec-ComboBoxAndComboboxEntry">
  <title>Controles de Lista Desplegable (ComboBox) y Lista Desplegable con Entrada (ComboBoxEntry)</title>

  <sect2 id="sec-ComboBox">
    <title>Controles ComboBox</title>

    <para>El control <classname>ComboBox</classname> sustituye el obsoleto 
<classname>OptionMenu</classname> con
un control potente que utiliza un <classname>TreeModel</classname> (generalmente un
<classname>ListStore</classname>) que proporciona los elementos de la lista que se mostrar�n.
El <classname>ComboBox</classname> implementa la interfaz
<classname>CellLayout</classname>, que proporcina diversos m�todos para gestionar la
visualizaci�n de los elementos de la lista. Uno o m�s
methods for managing the display of the list items. One or more
<classname>CellRenderer</classname>s se pueden empaquetar en un
<classname>ComboBox</classname> para personalizar la visualizaci�n de los elementos de
la lista.</para>

    <sect3 id="sec-BasicComboBox">
      <title>Uso B�sico de ComboBox</title>

      <para>La forma sencilla de crear y poblar un
<classname>ComboBox</classname> es utilizar la funci�n auxiliar:</para>

      <programlisting>
  combobox = gtk.combo_box_new_text()
</programlisting>

      <para>Esta funci�n crea una <classname>ComboBox</classname> y su almac�n
<classname>ListStore</classname> asociado y lo empaqueta con un
<classname>CellRendererText</classname>. Los siguientes m�todos auxiliares se usan
para poblar o eliminar los contenidos de la <classname>ComboBox</classname> y su
<classname>ListStore</classname>:</para>

      <programlisting>
  combobox.append_text(<parameter role="keyword">text</parameter>)
  combobox.append_text(<parameter role="keyword">text</parameter>)
  combobox.insert_text(<parameter role="keyword">position</parameter>, <parameter role="keyword">text</parameter>)
  combobox.remove_text(<parameter role="keyword">position</parameter>)
</programlisting>

      <para>donde <parameter>text</parameter> es la cadena que se a�adir� a la
<classname>ComboBox</classname> y <parameter>position</parameter> es el �ndice donde
se insertar� o eliminar� el texto <parameter>text</parameter>. En la mayor�a de los casos
las funciones y m�todos auxiliares es todo lo que se necesitar�.</para>

      <para>El programa de ejemplo <ulink
url="examples/comboboxbasic.py">comboboxbasic.py</ulink> demuestra el uso de las
anteriores funciones y m�todos. <xref
          linkend="comboboxbasicfig"></xref> ilustra el programa en ejecuci�n:</para>

      <figure id="comboboxbasicfig">
        <title>ComboBox B�sica</title>
        <mediaobject>
          <imageobject>
            <imagedata fileref="figures/comboboxbasic.png" format="png" align="center"></imagedata>
          </imageobject>
        </mediaobject>
      </figure>

      <para>Para obtener el texto activo se puede usar el m�todo:</para>

      <programlisting>
  texto = combobox.get_active_text()
      </programlisting>

      <para>Sin embargo, hasta la versi�n 2.6 no se proporciona en
          <literal>GTK</literal>+ un m�todo c�modo para obtener el texto activo.
          Para ello se podr�a usar una implementaci�n similar a:</para>

      <programlisting>
  def get_active_text(combobox):
      model = combobox.get_model()
      active = combobox.get_active()
      if active &lt; 0:
          return None
      return model[active][0]
</programlisting>

      <para>El �ndice del elemento activo se obtiene a trav�s del m�todo:</para>

      <programlisting>
  active = combobox.get_active()
</programlisting>

      <para>El elemento activo se puede establecer con el m�todo:</para>

      <programlisting>
  combobox.set_active(<parameter role="keyword">index</parameter>)
</programlisting>

      <para>donde <parameter>index</parameter> es un entero mayor que
-2. Si <parameter>index</parameter> es -1 no hay elemento activo y el control
<classname>ComboBox</classname> estar� en blanco. Si <parameter>index</parameter> es
menor que -1, la llamada ser� ignorada. Si <parameter>index</parameter> es mayor que -1
el elemento de la lista con dicho �ndice ser� mostrado.</para>

      <para>Se puede conectar a la se�al "changed" de un
<classname>ComboBox</classname> para recibir notificaci�n del cambio del elemento activo.
La signatura del manejador de "changed" es:</para>

      <programlisting>
  def changed_cb(combobox, ...):
</programlisting>

      <para>donde <parameter>...</parameter> representa cero o m�s argumentos pasados al
m�todo <methodname>GObject.connect</methodname>().</para>

    </sect3>

    <sect3 id="sec-AdvancedComboBox">
      <title>Uso Avanzado de ComboBox</title>

      <para>La creaci�n de una lista <classname>ComboBox</classname> mediante la funci�n
<function>gtk.combo_box_new_text</function>() es aproximadamente equivalente al
siguiente c�digo:</para>

      <programlisting>
  liststore = gtk.ListStore(str)
  combobox = gtk.ComboBox(liststore)
  cell = gtk.CellRendererText()
  combobox.pack_start(cell, True)
  combobox.add_attribute(cell, 'text', 0)  
</programlisting>

      <para>Para sacar partido de la potencia de las variadas clases de objetos
<classname>TreeModel</classname> y <classname>CellRenderer</classname>
es necesario construir una <classname>ComboBox</classname> utilizando el constructor:</para>

      <programlisting>
  combobox = gtk.ComboBox(<parameter role="keyword">model</parameter>=None)
</programlisting>

      <para>donde <parameter>model</parameter> es un modelo
<classname>TreeModel</classname>. Si se crea una lista
<classname>ComboBox</classname> sin asociarle un
<classname>TreeModel</classname> es posible a�adirlo a posteriori utilizando el m�todo:</para>

      <programlisting>
  combobox.set_model(<parameter role="keyword">model</parameter>)
</programlisting>

      <para>Se puede obtener el <classname>TreeModel</classname> asociado con el
m�todo:</para>

      <programlisting>
  model = combobox.get_model()
</programlisting>

      <para>Algunas de las cosas que se pueden hacer con una
<classname>ComboBox</classname> son:</para>

      <itemizedlist>
        <listitem>
          <simpara>Compartir el mismo <classname>TreeModel</classname> con otras
<classname>ComboBox</classname>es y
<classname>TreeView</classname>s.</simpara>
        </listitem>
        <listitem>
          <simpara>Mostrar im�genes y texto en los elementos de la
<classname>ComboBox</classname>.</simpara>
        </listitem>
        <listitem>
          <simpara>Utilizar un <classname>TreeStore</classname> o
<classname>ListStore</classname> existente como modelo para los elementos de la lista de la
<classname>ComboBox</classname>.</simpara>
        </listitem>
        <listitem>
          <simpara>Utilizar un <classname>TreeModelSort</classname> para disponer de una
lista de <classname>ComboBox</classname> ordenada.</simpara>
        </listitem>
        <listitem>
          <simpara>Utilizar un <classname>TreeModelFilter</classname> para usar un sub�rbol
de un <classname>TreeStore</classname> como fuente de elementos de la lista de la
<classname>ComboBox</classname>.</simpara>
        </listitem>
        <listitem>
          <simpara>Usar un <classname>TreeModelFilter</classname> para utilizar un subconjunto
de las filas de un <classname>TreeStore</classname> o
<classname>ListStore</classname> como elementos de la lista de la
<classname>ComboBox</classname>.</simpara>
        </listitem>
        <listitem>
          <simpara>Utilizar una funci�n de datos de celda para modificar o sintetizar la visualizaci�n
de los elementos de la lista.</simpara>
        </listitem>
      </itemizedlist>

      <para>El uso de los objetos <classname>TreeModel</classname> y
<classname>CellRenderer</classname> se detalla en el <link
linkend="ch-TreeViewWidget">cap�tulo de Controles de Vista de �rbol</link>.</para>

      <para>Los elementos de la lista de la <classname>ComboBox</classname> se pueden
mostrar en una tabla si se tiene un gran n�mero de elementos que visualizar. En otro caso,
la lista tendr� flechas de desplazamiento si la lista no puede ser mostrada en su totalidad. El
siguiente m�todo se usa para determinar el n�mero de columnas que se mostrar�n:</para>

      <programlisting>
  combobox.set_wrap_width(<parameter role="keyword">width</parameter>)
</programlisting>

      <para>donde <parameter>width</parameter> es el n�mero de columnas de la tabla que
muestra los elementos de la lista. Por ejemplo, el programa <ulink
url="examples/comboboxwrap.py">comboboxwrap.py</ulink> muestra una lista de 50 elementos
en 5 columnas. <xref linkend="comboboxwrapfig"></xref>
ilustra el programa en acci�n:</para>

      <figure id="comboboxwrapfig">
        <title>ComboBox con una Disposici�n Asociada</title>
        <mediaobject>
          <imageobject>
            <imagedata fileref="figures/comboboxwrap.png" format="png" align="center"></imagedata>
          </imageobject>
        </mediaobject>
      </figure>

      <para>Con un gran n�mero de elementos, digamos, por ejemplo, 50, el uso del m�todo
<methodname>set_wrap_width</methodname>() tendr� un mal rendimiento debido al c�lculo
de la disposici�n en tabla. Para tener una noci�n del efecto se puede modificar el programa
<ulink url="examples/comboboxwrap.py">comboboxwrap.py</ulink>
en su l�nea 18 de forma que muestre 150 elementos.</para>

      <programlisting>
        for n in range(150):
</programlisting>

      <para>Ejecute el programa para obtener una idea aproximada del tiempo de inicializaci�n.
Luego modif�quelo comentando la l�nea 17:</para>

      <programlisting>
        #combobox.set_wrap_width(5)
</programlisting>

      <para>Ejecute y cronometre de nuevo. Deber�a ejecutarse significativamente m�s r�pido.
Unas 20 veces m�s r�pido.</para>

      <para>Adem�s del m�todo <methodname>get_active</methodname>() antes descrito, se
puede obtener un iterador <classname>TreeIter</classname> que se�ala la fila activa mediante
el m�todo:</para>

      <programlisting>
  iter = combobox.get_active_iter()
</programlisting>

      <para>Tambi�n se puede establecer el elemento de la lista activa usando un iterador
<classname>TreeIter</classname> con el m�todo:</para>

      <programlisting>
  combobox.set_active_iter(<parameter role="keyword">iter</parameter>)
</programlisting>

      <para>Los m�todos <methodname>set_row_span_column</methodname>() y
<methodname>set_column_span_column</methodname>() permiten la especificaci�n de un
n�mero de columna de un <classname>TreeModel</classname> que contiene el n�mero de filas
o columnas que debe abarcar el elemento de la lista en una disposici�n de tabla. Desgraciadamente, en GTK+ 2.4 estos m�todos funcionan mal.</para>

      <para>Puesto que <classname>ComboBox</classname> implementa la interfaz
<classname>CellLayout</classname>, que tiene capacidades similares a las de una
<classname>TreeViewColumn</classname> (v�ase la <link
linkend="sec-TreeViewColumns">secci�n de TreeViewColumn</link> para m�s informaci�n).
En resumen, la interfaz proporciona:</para>

      <programlisting>
  combobox.pack_start(<parameter role="keyword">cell</parameter>, <parameter role="keyword">expand</parameter>=True)
  combobox.pack_end(<parameter role="keyword">cell</parameter>, <parameter role="keyword">expand</parameter>=True)
  combobox.clear()
</programlisting>

      <para>Los dos primeros m�todos empaquetan un <classname>CellRenderer</classname>
en la <classname>ComboBox</classname> y el m�todo <methodname>clear</methodname>()
elimina todos los atributos de todos los <classname>CellRenderer</classname>s.</para>

      <para>Los siguientes m�todos:</para>

      <programlisting>
  comboboxentry.add_attribute(<parameter role="keyword">cell</parameter>, <parameter role="keyword">attribute</parameter>, <parameter role="keyword">column</parameter>)

  comboboxentry.set_attributes(<parameter>cell</parameter>, <parameter>...</parameter>)
</programlisting>

      <para>establecen los atributos del <classname>CellRenderer</classname> indicado
por <parameter>cell</parameter>. El m�todo <methodname>add_attribute</methodname>()
toma una cadena de nombre de atributo <parameter>attribute</parameter> (p.e. 'text') y un
n�mero entero de columna <parameter>column</parameter> de la columna en el
<classname>TreeModel</classname> usado, para fijar el atributo
<parameter>attribute</parameter>. Los argumentos restante del m�todo
<methodname>set_attributes</methodname>() son pares
<literal>atributo=columna</literal> (p.e. text=1).</para>

    </sect3>

  </sect2>

  <sect2 id="sec-ComboBoxEntry">
    <title>Controles ComboBoxEntry</title>

    <para>El control <classname>ComboBoxEntry</classname> sustituye al control
<classname>Combo</classname>. Es una subclase del control
<classname>ComboBox</classname> y contiene un control hijo de entrada
<classname>Entry</classname> que obtiene sus contenidos seleccionando un elemento
en la lista desplegable o introduciendo texto directamente en la entrada bien desde el teclado
o peg�ndolo desde un portapapeles <classname>Clipboard</classname> o una selecci�n.</para>

    <sect3 id="sec-BasicComboBoxEntry">
      <title>Uso B�sico de ComboBoxEntry</title>

      <para>Como la <classname>ComboBox</classname>, la
<classname>ComboBoxEntry</classname> se puede crear con la funci�n auxiliar:</para>

      <programlisting>
  comboboxentry = gtk.combo_box_entry_new_entry()
</programlisting>

      <para>Una <classname>ComboBoxEntry</classname> se debe rellenar utilizando los
m�todos auxiliares de <classname>ComboBox</classname> descritos en
<link linkend="sec-BasicComboBox">Uso B�sico de ComboBox</link>.</para>

      <para>Puesto que un control <classname>ComboBoxEntry</classname> es un control
<classname>Bin</classname>, su control hijo de entrada <classname>Entry</classname>
est� disponible utilizando el atributo "child" o el m�todo
<methodname>get_child</methodname>():</para>

      <programlisting>
  entry = comboboxentry.child
  entry = comboboxentry.get_child()
</programlisting>

      <para>Se puede obtener el texto de la entrada <classname>Entry</classname> utilizando su
m�todo <methodname>get_text</methodname>().</para>

      <para>Al igual que <classname>ComboBox</classname>, es posible seguir los cambios del
elemento activo de la lista conect�ndose a la se�al "changed". Desgraciadamente, esto no 
permite seguir los cambios en la entrada <classname>Entry</classname> que se hacen por
entrada directa. Cuando se hace una entrada directa al control <classname>Entry</classname>
se emite la se�al "changed", pero el �ndice devuelto por el m�todo
<methodname>get_active</methodname>() ser� -1. Para seguir todos los cambios del texto
de la entrada <classname>Entry</classname> text, ser� necesario utilizar la se�al "changed"
de la entrada <classname>Entry</classname>. Por ejemplo:</para>

      <programlisting>
  def changed_cb(entry):
      print entry.get_text()

  comboboxentry.child.connect('changed', changed_cb)
</programlisting>

      <para>imprimir� el texto tras cada cambio en el control hijo
<classname>Entry</classname>. Por ejemplo, el programa <ulink
url="examples/comboboxentrybasic.py">comboboxentrybasic.py</ulink> muestra el uso
de la API auxiliar. <xref
          linkend="comboboxentrybasicfig"></xref> ilustra la ejecuci�n del programa:</para>

      <figure id="comboboxentrybasicfig">
        <title>ComboBoxEntry B�sica</title>
        <mediaobject>
          <imageobject>
            <imagedata fileref="figures/comboboxentrybasic.png" format="png" align="center"></imagedata>
          </imageobject>
        </mediaobject>
      </figure>

      <para>Obs�rvese que cuando se modifica el texto de la entrada
<classname>Entry</classname> debido a la selecci�n de un elemento de la lista desplegable
se llama dos veces al manejador de la se�al "changed": una vez cuando se elimina el texto, y,
otra cuando el texto se establece desde el elemento seleccionado de la lista.</para>

    </sect3>

    <sect3 id="sec=AdvancedComboBoxEntry">
      <title>Uso Avanzado de ComboBoxEntry</title>

      <para>El constructor de una ComboBoxEntry es:</para>

      <programlisting>
  comboboxentry = gtk.ComboBoxEntry(<parameter role="keyword">model</parameter>=None, <parameter role="keyword">column</parameter>=-1)
</programlisting>

      <para>donde <parameter>model</parameter> es un
<classname>TreeModel</classname> y <parameter>column</parameter> es el n�mero de la
columna en el modelo <parameter>model</parameter> que se usar� para fijar los elementos de
la lista. Si no se indica la columna el valor predeterminado es -1 que significa que el texto de la
columna no est� especificado.</para>

      <para>La creaci�n de una <classname>ComboBoxEntry</classname> utilizando la funci�n
auxiliar <function>gtk.combo_box_entry_new_text</function>() es equivalente al siguiente
c�digo:</para>

      <programlisting>
  liststore = gtk.ListStore(str)
  comboboxentry = gtk.ComboBoxEntry(liststore, 0)
</programlisting>

      <para>La <classname>ComboBoxEntry</classname> a�ade un par de m�todos que se usan
para establecer y recuperar el n�mero de columna del <classname>TreeModel</classname>
que se usar� para fijar las cadenas de los elementos de la lista:</para>

      <programlisting>
  comboboxentry.set_text_column(<parameter role="keyword">text_column</parameter>)
  text_column = comboboxentry.get_text_column()
</programlisting>

      <para>La columna de texto tambi�n se puede obtener y especificar utilizando la propiedad
"text-column". V�ase la <link
linkend="sec-AdvancedComboBox">Secci�n de Uso Avanzado de ComboBox</link> para m�s
informaci�n sobre el uso avanzado de una <classname>ComboBoxEntry</classname>.</para>

      <note>
        <para>La aplicaci�n debe establecer la columna de texto para que la
<classname>ComboBoxEntry</classname> fije los contenidos de la entrada
<classname>Entry</classname> desde la lista desplegable. La columna de texto �nicamente
puede determinarse una vez, bien utilizando el constructor o utilizando el m�todo
<methodname>set_text_column</methodname>().</para>
      </note>

      <para>Al crear una <classname>ComboBoxEntry</classname> �sta se empaqueta con un
nuevo <classname>CellRendererText</classname> que no es accesible. El atributo
'text' del <classname>CellRendererText</classname> se establece como un efecto colateral
de la determinaci�n de la columna de texto utilizando el m�todo
<methodname>set_text_column</methodname>(). Se pueden empaquetar <classname>CellRenderer</classname>s adicionales en una
<classname>ComboBoxEntry</classname> para la visualizaci�n en la lista desplegable. V�ase
la <link linkend="sec-AdvancedComboBox">Secci�n de Uso Avanzado de ComboBox</link>
para m�s informaci�n.</para>

    </sect3>

  </sect2>

</sect1>
