<?xml version="1.0" encoding="iso-8859-1" standalone="no"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
    "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">

<sect1 id="sec-GenericTreeModel">
  <title>El Modelo de �rbol Gen�rico (GenericTreeModel)</title>

  <para>En el momento en el que se encuentra que los modelo de �rbol est�ndar
<classname>TreeModel</classname>s no son suficientemente potentes para cubrir las
necesidades de una aplicaci�n se puede usar la clase
<classname>GenericTreeModel</classname> para construir modelos
<classname>TreeModel</classname> personalizados en Python. La creaci�n de un
<classname>GenericTreeModel</classname> puede ser de utilidad cuando existen problemas
de rendimiento con los almacenes est�ndar <classname>TreeStore</classname> y
<classname>ListStore</classname> o cuando se desea tener una interfaz directa con una
fuente externa de datos (por ejemplo, una base de datos o el sistema de archivos) para evitar
la copia de datos dentro y fuera de los almacenes <classname>TreeStore</classname> o
<classname>ListStore</classname>.</para>

  <sect2 id="sec-GenericTreeModelOverview">
    <title>Visi�n general de GenericTreeMode</title>

    <para>Con <classname>GenericTreeModel</classname> se construye y gestiona un modelo
propio de datos y se proporciona acceso externo a trav�s de la interfaz est�ndar
<classname>TreeModel</classname> al definir un conjunto de m�todos de clase. PyGTK
implementa la interfaz <classname>TreeModel</classname> y hace que los nuevos m�todos
<classname>TreeModel</classname> sean llamados para proporcionar los datos del modelo 
existente.</para>

    <para>Los detalles de implementaci�n del modelo personalizado deben mantenerse ocultos a
la aplicaci�n externa. Ello significa que la forma en la que se identifica el modelo, guarda y obtiene 
sus datos es desconocido a la aplicaci�n. En general, la �nica informaci�n que se guarda
fuera del <classname>GenericTreeModel</classname> son las referencia de fila que se 
encapsulan por los iteradores <classname>TreeIter</classname> externos. Y estas referencias no son visibles a la aplicaci�n.</para>

    <para>Examinemos en detalle la interfaz <classname>GenericTreeModel</classname>
que es necesario proporcionar.</para>

  </sect2>

  <sect2 id="sec-GenericTreeModelInterface">
    <title>La Interfaz GenericTreeModel</title>

    <para>La interfaz <classname>GenericTreeModel</classname> consiste en los siguientes
m�todos que deben implementarse en el modelo de �rbol personalizado:</para>

    <methodsynopsis language="python">
      <methodname>on_get_flags</methodname>
      <methodparam><parameter>self</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>on_get_n_columns</methodname>
      <methodparam><parameter>self</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>on_get_column_type</methodname>
      <methodparam><parameter>self</parameter></methodparam>
      <methodparam><parameter>index</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>on_get_iter</methodname>
      <methodparam><parameter>self</parameter></methodparam>
      <methodparam><parameter>path</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>on_get_path</methodname>
      <methodparam><parameter>self</parameter></methodparam>
      <methodparam><parameter>rowref</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>on_get_value</methodname>
      <methodparam><parameter>self</parameter></methodparam>
      <methodparam><parameter>rowref</parameter></methodparam>
      <methodparam><parameter>column</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>on_iter_next</methodname>
      <methodparam><parameter>self</parameter></methodparam>
      <methodparam><parameter>rowref</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>on_iter_children</methodname>
      <methodparam><parameter>self</parameter></methodparam>
      <methodparam><parameter>parent</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>on_iter_has_child</methodname>
      <methodparam><parameter>self</parameter></methodparam>
      <methodparam><parameter>rowref</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>on_iter_n_children</methodname>
      <methodparam><parameter>self</parameter></methodparam>
      <methodparam><parameter>rowref</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>on_iter_nth_child</methodname>
      <methodparam><parameter>self</parameter></methodparam>
      <methodparam><parameter>parent</parameter></methodparam>
      <methodparam><parameter>n</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>on_iter_parent</methodname>
      <methodparam><parameter>self</parameter></methodparam>
      <methodparam><parameter>child</parameter></methodparam>
    </methodsynopsis>
 
    <para>Debe advertirse que estos m�todos dan soporte a toda la interfaz de
<classname>TreeModel</classname> inclu�da:</para>
    
    <methodsynopsis language="python">
      <methodname>get_flags</methodname>
      <methodparam></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>get_n_columns</methodname>
      <methodparam></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>get_column_type</methodname>
      <methodparam><parameter role="keyword">index</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>get_iter</methodname>
      <methodparam><parameter role="keyword">path</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>get_iter_from_string</methodname>
      <methodparam><parameter role="keyword">path_string</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>get_string_from_iter</methodname>
      <methodparam><parameter>iter</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>get_iter_root</methodname>
      <methodparam></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>get_iter_first</methodname>
      <methodparam></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>get_path</methodname>
      <methodparam><parameter role="keyword">iter</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>get_value</methodname>
      <methodparam><parameter role="keyword">iter</parameter></methodparam>
      <methodparam><parameter role="keyword">column</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>iter_next</methodname>
      <methodparam><parameter role="keyword">iter</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>iter_children</methodname>
      <methodparam><parameter role="keyword">parent</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>iter_has_child</methodname>
      <methodparam><parameter role="keyword">iter</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>iter_n_children</methodname>
      <methodparam><parameter role="keyword">iter</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>iter_nth_child</methodname>
      <methodparam><parameter role="keyword">parent</parameter></methodparam>
      <methodparam><parameter role="keyword">n</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>iter_parent</methodname>
      <methodparam><parameter role="keyword">child</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>get</methodname>
      <methodparam><parameter>iter</parameter></methodparam>
      <methodparam><parameter>column</parameter></methodparam>
      <methodparam><parameter>...</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>foreach</methodname>
      <methodparam><parameter>func</parameter></methodparam>
      <methodparam><parameter>user_data</parameter></methodparam>
    </methodsynopsis>

    <para>Para ilustrar el uso de
<classname>GenericTreeModel</classname> modificaremos el programa de ejemplo <ulink
url="examples/filelisting.py"><command>filelisting.py</command></ulink>
y veremos c�mo se crean los m�todos de la interfaz. El programa <ulink
url="examples/filelisting-gtm.py"><command>filelisting-gtm.py</command></ulink>
muestra los archivos de una carpeta con un pixbuf que indica si el archivo es una carpeta o no,
el nombre de archivo, el tama�o del mismo, el modo y hora del �ltimo cambio.</para>

    <para>El m�todo <methodname>on_get_flags</methodname>() deber�a devolver un valor
que es una combinaci�n de:</para>

    <variablelist>
      <varlistentry>
        <term><literal>gtk.TREE_MODEL_ITERS_PERSIST</literal></term>
        <listitem>
          <simpara>Los <classname>TreeIter</classname>s sobreviven a todas las se�ales
emitidas por el �rbol.</simpara>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><literal>gtk.TREE_MODEL_LIST_ONLY</literal></term>
        <listitem>
          <simpara>El modelo es solamente una lista, y nunca tiene hijos</simpara>
        </listitem>
      </varlistentry>
    </variablelist>

    <para>Si el modelo tiene referencias de fila que son v�lidas entre cambios de fila
(reordenaci�n, adici�n o borrado) entonces se dbe establecer
<literal>gtk.TREE_MODEL_ITERS_PERSIST</literal>. Igualmente, si el modelo es una lista
entonces de debe fijar <literal>gtk.TREE_MODEL_LIST_ONLY</literal>. De otro modo,
se debe devolver 0 si el modelo no tiene referencias de fila persistentes y es un modelo
de �rbol. Para nuestro ejemplo, el modelo es una lista con iteradores
<classname>TreeIter</classname> persitentes.</para>

    <programlisting>
    def on_get_flags(self):
        return gtk.TREE_MODEL_LIST_ONLY|gtk.TREE_MODEL_ITERS_PERSIST
</programlisting>

    <para>El m�todo <methodname>on_get_n_columns</methodname>() deber�a devolver
el n�mero de columnas que el modelo exporta a la aplicaci�n. Nuestro ejemplo mantiene una lista
de los tipos de las columnas, de forma que devolvemos la longitud de la lista:</para>

    <programlisting>
class FileListModel(gtk.GenericTreeModel):
    ...
    column_types = (gtk.gdk.Pixbuf, str, long, str, str)
    ...
    def on_get_n_columns(self):
        return len(self.column_types)
</programlisting>

    <para>El m�todo <methodname>on_get_column_type</methodname>() deber�a devolver el
tipo de la columna con el valor de �ndice <parameter>index</parameter> especificado. Este
m�todo es llamado generalmente desde una <classname>TreeView</classname> cuando
se establece su modelo. Se puede, bien crear una lista o tupla que contenga la informaci�n
de tipos de datos de las columnas o bien generarla al vuelo. En nuestro ejemplo:</para>

    <programlisting>
    def on_get_column_type(self, n):
        return self.column_types[n]
</programlisting>

    <para>La interfaz <classname>GenericTreeModel</classname> convierte el tipo de Python
a un <classname>GType</classname>, de forma que el siguiente c�digo:</para>

    <programlisting>
  flm = FileListModel()
  print flm.on_get_column_type(1), flm.get_column_type(1)
</programlisting>

    <para>mostrar�a:</para>

    <programlisting>
&lt;type 'str'&gt; &lt;GType gchararray (64)&gt;
</programlisting>

    <para>Los siguientes m�todos usan referencias de fila que se guardan como datos privados
en un <classname>TreeIter</classname>. La aplicaci�n no puede ver la referencia de fila en un
<classname>TreeIter</classname> por lo que se puede usar cualquier elemento �nico que se
desee como referencia de fila. Por ejemplo, en un modelo que contiene filas como tuplas, se
podr�a usar la identificaci�n de tupla como referencia de la fila. Otro ejemplo ser�a el uso de
un nombre de archivo como referencia de fila en un modelo que represente los archivos de
un directorio. En ambos casos la referencia de fila no se modifica con los cambios en el modelo,
as� que los <classname>TreeIter</classname>s se podr�an marcar como persistentes. La
interfaz de aplicaci�n de PyGTK <classname>GenericTreeModel</classname> extraer�
esas referencias de fila desde los <classname>TreeIter</classname>s y encapsular�
las referencias de fila en <classname>TreeIter</classname>s seg�n sea necesario.</para>

    <para>En los siguientes m�todos <literal>rowref</literal> se refiere a una referencia de
fila interna.</para>

    <para>El m�todo <methodname>on_get_iter</methodname>() deber�a devolver una
rowref del camino de �rbol indicado por <parameter>path</parameter>. El camino de �rbol se
representar� siempre mediante una tupla. Nuestro ejemplo usa la cadena del nombre de archivo
como rowref. Los nombres de archivo se guardan en una lista en el modelo, de manera que
tomamos el primer �ndice del camino como �ndice al nombre de archivo:</para>

    <programlisting>
    def on_get_iter(self, path):
        return self.files[path[0]]
</programlisting>

    <para>Es necesario ser coherente en el uso de las referencias de fila, puesto que
se obtendr�n referencias de fila tras las llamadas a los m�todos de
<classname>GenericTreeModel</classname> que toman argumentos con iteradores
<classname>TreeIter</classname>:
<methodname>on_get_path</methodname>(),
<methodname>on_get_value</methodname>(),
<methodname>on_iter_next</methodname>(),
<methodname>on_iter_children</methodname>(),
<methodname>on_iter_has_child</methodname>(),
<methodname>on_iter_n_children</methodname>(),
<methodname>on_iter_nth_child</methodname>() y
<methodname>on_iter_parent</methodname>().</para>

    <para>El m�todo <methodname>on_get_path</methodname>() deber�a devolver un camino
de �rbol dada una rowref. Por ejemplo, siguiendo con el ejemplo anterior donde el nombre de
archivo se usa como rowref, se podr�a definir el m�todo
<methodname>on_get_path</methodname>() as�:</para>

    <programlisting>
    def on_get_path(self, rowref):
        return self.files.index(rowref)
</programlisting>

    <para>Este m�todo localiza el �ndice de la lista que contiene el nombre de archivo en
<parameter>rowref</parameter>. Es obvio viendo este ejemplo que una elecci�n juiciosa de
la referencia de fila har� la implementaci�n m�s eficiente. Se podr�a usar, por ejemplo, un
diccionario de Python para traducir una
<parameter>rowref</parameter> a un camino.</para>

    <para>El m�todo <methodname>on_get_value</methodname>() deber�a devolver los datos
almacenados en la fila y columna especificada por <parameter>rowref</parameter> y
la columna <parameter>column</parameter>. En nuestro ejemplo:</para>

    <programlisting>
    def on_get_value(self, rowref, column):
        fname = os.path.join(self.dirname, rowref)
        try:
            filestat = statcache.stat(fname)
        except OSError:
            return None
        mode = filestat.st_mode
        if column is 0:
            if stat.S_ISDIR(mode):
                return folderpb
            else:
                return filepb
        elif column is 1:
            return rowref
        elif column is 2:
            return filestat.st_size
        elif column is 3:
            return oct(stat.S_IMODE(mode))
        return time.ctime(filestat.st_mtime)
</programlisting>

    <para>tiene que extraer la informaci�n de archivo asociada y devolver el valor adecuado en
funci�n de qu� columna se especifique.</para>

    <para>El m�todo <methodname>on_iter_next</methodname>() deber�a devolver una rerferencia
de fila a la fila (en el mismo nivel) posterior a la especificada por<parameter>rowref</parameter>.
En nuestro ejemplo:</para>

    <programlisting>
    def on_iter_next(self, rowref):
        try:
            i = self.files.index(rowref)+1
            return self.files[i]
        except IndexError:
            return None
</programlisting>

    <para>El �ndice del nombre de archivo <parameter>rowref</parameter> es determinado y se
devuelve el siguiente nombre de archivo o <literal>None</literal> si no existe un archivo
despu�s.</para>

    <para>El m�todo <methodname>on_iter_children</methodname>() deber�a devolver una
referencia de fila a la primera fila hija de la fila especificada por
<parameter>rowref</parameter>. Si <parameter>rowref</parameter> es
<literal>None</literal> entonces se devuelve una referencia a la primera fila de nivel superior. Si
no existe una fila hija se devuelve <literal>None</literal>. En nuestro ejemplo:</para>

    <programlisting>
    def on_iter_children(self, rowref):
        if rowref:
            return None
        return self.files[0]
</programlisting>

    <para>Puesto que el modelo es una lista �nicamente la fila superior puede tener filas hijas
(<parameter>rowref</parameter>=<literal>None</literal>). Se devuelve <literal>None</literal> si
<parameter>rowref</parameter> contiene un nombre de archivo.</para>

    <para>El m�todo <methodname>on_iter_has_child</methodname>() deber�a devolver
<literal>TRUE</literal> si la fila especificada por <parameter>rowref</parameter> tiene filas
hijas o <literal>FALSE</literal> en otro caso. Nuestro ejemplo devuelve <literal>FALSE</literal>
puesto que ninguna fila puede tener hijas:</para>

    <programlisting>
    def on_iter_has_child(self, rowref):
        return False
</programlisting>

    <para>El m�todo <methodname>on_iter_n_children</methodname>() deber�a devolver el
n�mero de fijas hijas que tiene la fila especificada por <parameter>rowref</parameter>. Si
<parameter>rowref</parameter> es <literal>None</literal> entonces se devuelve el n�mero
de las filas de nivel superior. Nuestro ejemplo devuelve 0 si <parameter>rowref</parameter>
no es <literal>None</literal>:</para>

    <programlisting>
    def on_iter_n_children(self, rowref):
        if rowref:
            return 0
        return len(self.files)
</programlisting>

    <para>El m�todo <methodname>on_iter_nth_child</methodname>() deber�a devolver una
referencia de fila a la n-�sima fila hija de la fila especificada por
<parameter>parent</parameter>. Si <parameter>parent</parameter> es
<literal>None</literal> entonces se devuelve una referencia a la n-�sima fila de nivel
superior. Nuestro ejemplo devuelve la n-�sima fila de nivel superior si
<parameter>parent</parameter> es <literal>None</literal>. En otro caso devuelve
<literal>None</literal>:</para>

    <programlisting>
    def on_iter_nth_child(self, rowref, n):
        if rowref:
            return None
        try:
            return self.files[n]
        except IndexError:
            return None
</programlisting>

    <para>El m�todo <methodname>on_iter_parent</methodname>() deber�a devolver una
referencia de fila a la fila padre de la fila especificada por <parameter>rowref</parameter>. Si
<parameter>rowref</parameter> apunta a una fila de nivel superior se debe devolver
<literal>None</literal>. Nuestro ejemplo siempre devuelve <literal>None</literal>
asumiendo que <parameter>rowref</parameter> debe apuntar a una fila de nivel superior:</para>

    <programlisting>
    def on_iter_parent(child):
        return None
</programlisting>

    <para>Este ejemplo se ve de una vez en el programa <ulink
url="examples/filelisting-gtm.py">filelisting-gtm.py</ulink>. <xref linkend="filelistinggtmfig"/>
muestra el resultado de la ejecuci�n del programa.</para>

    <figure id="filelistinggtmfig">
      <title>Programa de Ejemplo de Modelo de �rbol Gen�rico</title>
      <mediaobject>
        <imageobject>
          <imagedata fileref="figures/filelisting-gtm.png" format="png"
            align="center"/>
        </imageobject>
      </mediaobject>
    </figure>
    
  </sect2>

  <sect2 id="sec-AddRemoveGenericTreeModelRows">
    <title>Adici�n y Eliminaci�n de Filas</title>

    <para>El programa <ulink
url="examples/filelisting-gtm.py">filelisting-gtm.py</ulink> calcula la lista de nombres de archivo
mientras se crea una instancia de <classname>FileListModel</classname>. Si se desea
comprobar la existencia de nuevos archivos de forma peri�dica y a�adir o eliminar archivos del
modelo se podr�a, bien crear un nuevo modelo <classname>FileListModel</classname> de la
misma carpeta, o bien se podr�an incorporar m�todos para a�adir y eliminar filas del modelo.
Dependiendo del tipo de modelo que se est� creando se necesitar�a a�adir unos m�todos
similares a los de los modelos <classname>TreeStore</classname> y
<classname>ListStore</classname>:</para>

    <itemizedlist>
      <listitem>
        <simpara><methodname>insert</methodname>()</simpara>
      </listitem>
      <listitem>
        <simpara><methodname>insert_before</methodname>()</simpara>
      </listitem>
      <listitem>
        <simpara><methodname>insert_after</methodname>()</simpara>
      </listitem>
      <listitem>
        <simpara><methodname>prepend</methodname>()</simpara>
      </listitem>
      <listitem>
        <simpara><methodname>append</methodname>()</simpara>
      </listitem>
      <listitem>
        <simpara><methodname>remove</methodname>()</simpara>
      </listitem>
      <listitem>
        <simpara><methodname>clear</methodname>()</simpara>
      </listitem>
    </itemizedlist>

    <para>Naturalmente, ni todos ni cualquiera de estos m�todos neceista ser implementado. Se
pueden crear m�todos propios que se relacionen de manera m�s ajustada al modelo.</para>

    <para>Utilizando el programa de ejemplo anterior para ilustrar la adici�n de m�todos para
eliminar y a�adir archivos, implementemos esos m�todos:</para>

    <methodsynopsis language="python">
      <methodname>remove</methodname>
      <methodparam><parameter
                     role="keyword">iter</parameter></methodparam>
    </methodsynopsis>
    <methodsynopsis language="python">
      <methodname>add</methodname>
      <methodparam><parameter
                     role="keyword">filename</parameter></methodparam>
    </methodsynopsis>

    <para>El m�todo <methodname>remove</methodname>() elimina el archivo especificado por
<parameter>iter</parameter>. Adem�s de eliminar la fila del modelo el m�todo tambi�n deber�a
eliminar el archivo de la carpeta. Naturalmente, si el usuario no tiene permisos para eliminar
el archivo no se deber�a eliminar tampoco la fila. Por ejemplo:</para>

    <programlisting>
    def remove(self, iter):
        path = self.get_path(iter)
        pathname = self.get_pathname(path)
        try:
            if os.path.exists(pathname):
                os.remove(pathname)
            del self.files[path[0]]
            self.row_deleted(path)
        except OSError:
            pass
        return
</programlisting>

    <para>Al m�todo se le pasa un iterador <classname>TreeIter</classname> que ha de ser
convertido a un camino que se usa para obtener el camino del archivo usando el m�todo
<methodname>get_pathname</methodname>(). Es posible que el archivo ya haya sido eliminado
por lo que debemos comprobar si existe antes de intentar su eliminaci�n. Si se emite una
excepci�n OSError durante la eliminaci�n del archivo, probablemente se deba a que es un
directorio o que la usuaria no tiene los privilegios necesarios para elimnarlo. Finalmente, el archivo
se elimina y la se�al "row-deleted" se emite desde el m�todo
<methodname>rows_deleted</methodname>(). La se�al "file-deleted" notifica a las <classname>TreeView</classname>s que usan el modelo que �ste ha cambiado, por lo que
pueden actualizar su estado interno y mostrar el modelo actualizado.</para>

    <para>El m�todo <methodname>add</methodname>() necesita crear un archivo con el nombre
dado en la carpeta actual. Si se crea el archivo su nombre se a�ade a la lista de archivos del
modelo. Por ejemplo:</para>

    <programlisting>
    def add(self, filename):
        pathname = os.path.join(self.dirname, filename)
        if os.path.exists(pathname):
            return
        try:
            fd = file(pathname, 'w')
            fd.close()
            self.dir_ctime = os.stat(self.dirname).st_ctime
            files = self.files[1:] + [filename]
            files.sort()
            self.files = ['..'] + files
            path = (self.files.index(filename),)
            iter = self.get_iter(path)
            self.row_inserted(path, iter)
        except OSError:
            pass
        return
</programlisting>

    <para>Este ejemplo sencillo se asegura de que el archivo no existe y luego intenta abrir el 
archivo para escritura. Si tiene �xito, el archivo se cierra y el nombre de archivo ordenado en
la lista de archivos. La ruta y el iterador
<classname>TreeIter</classname> de la fila de archivo a�adida se obtienen para usarlos en el
m�todo <methodname>row_inserted</methodname>() que emite la se�al "row-inserted". La
se�al "row-inserted" se usa para notificar a las <classname>TreeView</classname>s que
usan el modelo que necesitan actualizar su estado interno y revisar su visualizaci�n.</para>

    <para>Los otros m�todos mencionados anteriormente (por ejemplo,
<methodname>append</methodname> y <methodname>prepend</methodname>) no tienen
sentido en el ejemplo, puesto que el modelo mantiene la lista de archivos ordenada.</para>

    <para>Otros m�todos que puede merecer la pena implementar en un
<classname>TreeModel</classname> que herede de
<classname>GenericTreeModel</classname> son:</para>

    <itemizedlist>
      <listitem>
        <simpara><methodname>set_value</methodname>()</simpara>
      </listitem>
      <listitem>
        <simpara><methodname>reorder</methodname>()</simpara>
      </listitem>
      <listitem>
        <simpara><methodname>swap</methodname>()</simpara>
      </listitem>
      <listitem>
        <simpara><methodname>move_after</methodname>()</simpara>
      </listitem>
      <listitem>
        <simpara><methodname>move_before</methodname>()</simpara>
      </listitem>
    </itemizedlist>

    <para>La implementaci�n de estos m�todos es similar a de los m�todos anteriores. Es
necesario sincronizar el modelo con el estado externo y luego notificar a las
<classname>TreeView</classname>s cuando el modelo cambie. Los siguientes m�todos
se usan para notificar a las <classname>TreeView</classname>s de cambios en el modelo
emitiendo la se�al apropiada:</para>

    <methodsynopsis language="python">
      <methodname>row_changed</methodname>
      <methodparam><parameter
                     role="keyword">path</parameter></methodparam>
      <methodparam><parameter
                     role="keyword">iter</parameter></methodparam>
    </methodsynopsis> # fila cambi�
    <methodsynopsis language="python">
      <methodname>row_inserted</methodname>
      <methodparam><parameter role="keyword">path</parameter></methodparam>
      <methodparam><parameter role="keyword">iter</parameter></methodparam>
    </methodsynopsis> # fila insertada
    <methodsynopsis language="python">
      <methodname>row_has_child_toggled</methodname>
      <methodparam><parameter role="keyword">path</parameter></methodparam>
      <methodparam><parameter role="keyword">iter</parameter></methodparam>
    </methodsynopsis> # cambio en la existencia de hijas en la fila
    <methodsynopsis language="python">
      <methodname>row_deleted</methodname>
      <methodparam><parameter role="keyword">path</parameter></methodparam>
    </methodsynopsis> # fila eliminada
    <methodsynopsis language="python">
      <methodname>rows_reordered</methodname>
      <methodparam><parameter role="keyword">path</parameter></methodparam>
      <methodparam><parameter role="keyword">iter</parameter></methodparam>
      <methodparam><parameter role="keyword">new_order</parameter></methodparam>
    </methodsynopsis> # filas reordenadas

    <para></para>

  </sect2>

  <sect2 id="sec-GenericTreeModelMemoryManagement">
    <title>Gesti�n de Memoria</title>

    <para>Uno de los problemas de <classname>GenericTreeModel</classname> es que
los <classname>TreeIter</classname>s guardan una referencia a un objeto de Python devuelto
por el modelo de �rbol personalizado. Puesto que se puede crear e inicializar un
<classname>TreeIter</classname> en c�digo en C y que permanezca en la pila, no es posible
saber cu�ndo se ha destruido el <classname>TreeIter</classname> y ya no se usa la referencia
al objeto de Python. Por lo tanto, el objeto de Python referenciado en un
<classname>TreeIter</classname> incrementa por defecto su cuenta de referencias, pero no se
decrementa cuando se destruye el <classname>TreeIter</classname>. Esto asegura que
el objeto de Python no ser� destruido mientras est� en uso por un
<classname>TreeIter</classname>, lo que podr�a causar una violaci�n de segmento.
Desgraciadamente, la cuenta de referencias extra lleva a la situaci�n en la que, como bueno, el
objeto de Python tendr� un contador de referencias excesivo, y como malo, nunca se liberar� esa
memoria incluso cuando ya no se usa. Este �ltimo caso lleva a p�rdidas de memoria y el primero a
p�rdidas de referencias.</para>

    <para>Para prever la situaci�n en la que el <classname>TreeModel</classname> personalizado
mantiene una referencia al objeto de Python hasta que ya no se dispone de �l (es decir, el
<classname>TreeIter</classname> ya no es v�lido porque el modelo ha cambiado)
y no hay necesidad de perder referencias, el <classname>GenericTreeModel</classname> tiene
la propiedad "leak-references". Por defecto "leak-references" es <literal>TRUE</literal> para
indicar que el <classname>GenericTreeModel</classname> pierde referencias. Si
"leak-references" se establece a <literal>FALSE</literal> entonces el contador de referencias
del objeto de Python no se incrementar� cuando se referencia en un
<classname>TreeIter</classname>. Esto implica que el <classname>TreeModel</classname>
propio debe mantener una referencia a todos los objetos de Python utilizados en los
<classname>TreeIter</classname>s hasta que el modelo sea destruido. Desgraciadamente, 
incluso esto no permite la protecci�n frente a c�digo err�neo que intenta utilizar un 
<classname>TreeIter</classname> guardado en un <classname>GenericTreeModel</classname>
diferente. Para protegerse frente a este caso la aplicaci�n deber�a mantener referencias
a todos los objetos de Python referenciados desde un <classname>TreeIter</classname> desde
cualquier instancia de un <classname>GenericTreeModel</classname>. Naturalmente, esto
tiene finalmente el mismo resultado que la p�rdida de referencias.</para>

    <para>En PyGTK 2.4 y posteriores los m�todos
<methodname>invalidate_iters</methodname>() y
<methodname>iter_is_valid</methodname>() est�n disponibles para ayudar en la gesti�n de
los <classname>TreeIter</classname>s y sus referencias a objetos de Python:</para>

    <programlisting>
  generictreemodel.invalidate_iters()

  result = generictreemodel.iter_is_valid(<parameter role="keyword">iter</parameter>)
</programlisting>

    <para>Estas son particularmente �tiles cuando la propiedad "leak-references" est� fijada como
<literal>FALSE</literal>. Los modelos de �rbol derivados de
<classname>GenericTreeModel</classname> est�n protegidos de problemas con
<classname>TreeIters</classname> obsoletos porque se comprueba autom�ticamente la validez
de los iteradores con el modelo de �rbol.</para>

    <para>Si un modelo de �rbol personalizado no soporta iteradores persistentes
(es decir, <literal>gtk.TREE_MODEL_ITERS_PERSIST</literal> no est� activado en el resultado del
m�todo <methodname>TreeModel.get_flags</methodname>() ) entonces puede llamar al m�todo
<methodname>invalidate_iters</methodname>() para invalidar los
<classname>TreeIter</classname>s restantes cuando cambia el modelo (por ejemplo, tras insertar
una fila). El modelo de �rbol tambi�n puede deshacerse de cualquier objeto de Python que fue
referenciado por <classname>TreeIter</classname>s tras llamar al m�todo
<methodname>invalidate_iters</methodname>().</para>

    <para>Las aplicaciones pueden usar el m�todo <methodname>iter_is_valid</methodname>()
para determinar si un <classname>TreeIter</classname> es a�n v�lido en el modelo 
personalizado.</para>

  </sect2>

  <sect2 id="sec-OtherInterfaces">
    <title>Otras Interfaces</title>

    <para>Los modelos <classname>ListStore</classname> y
<classname>TreeStore</classname> soportan las interfaces
<classname>TreeSortable</classname>, <classname>TreeDragSource</classname>
y <classname>TreeDragDest</classname> adem�s de la interfaz
<classname>TreeModel</classname>. La clase
<classname>GenericTreeModel</classname> unicamente soporta la interfaz
<classname>TreeModel</classname>. Esto parece ser as� dada la referencia directa al modelo
en el nivel de C por las vistas <classname>TreeView</classname> y los modelos
<classname>TreeModelSort</classname> y
<classname>TreeModelFilter</classname> models. La creaci�n y uso de
<classname>TreeIter</classname>s precisa c�digo de uni�n en C que haga de enlace con
el modelo de �rbol personalizado en Python que tiene los datos. Ese c�digo de conexi�n lo aporta
la clase <classname>GenericTreeModel</classname> y parece que no hay una forma alternativa
de hacerlo puramente en Python puesto que las
<classname>TreeView</classname>s y los otros modelo llaman a las funciones de GtkTreeModel
en C y pasan sus referencias al modelo de �rbol personalizado.</para>

    <para>La interfaz <classname>TreeSortable</classname> tambi�n necesitar�a c�digo de
enlace en C  para funcionar con el mecanismo de ordenaci�n predeterminado de
<classname>TreeViewColumn</classname>, que se explica en la secci�n
<link linkend="sec-SortingTreeModelRows">Ordenaci�n de Filas del Modelo de �rbol</link>.
Sin embargo, un modelo personalizado puede hacer su propia ordenaci�n y una aplicaci�n
puede gestionar el uso de los criterios de ordenaci�n manejando los clic sobre las cabeceras
de las <classname>TreeViewColumn</classname>s y llamando a los m�todos personalizados
de ordenaci�n del modelo. El modelo completa la actualizaci�n de las vistas
<classname>TreeView</classname> emitiendo la se�al "rows-reordered" utilizando el m�todo
de <classname>TreeModel</classname> <methodname>rows_reordered</methodname>().
As�, probablemente no es necesario que <classname>GenericTreeModel</classname> implemente
la interfaz <classname>TreeSortable</classname>.</para>

    <para>De la misma manera, la clase <classname>GenericTreeModel</classname> no necesita
implementar las interfaces <classname>TreeDragSource</classname> y
<classname>TreeDragDest</classname> puesto que el modelo de �rbol personalizado puede
implementar sus propias interfaces de arrastrar y soltar y la aplicaci�n puede majenar las se�ales
de <classname>TreeView</classname> adecuadas y llamar a los m�todos propios del modelo
seg�n sea necesario.</para>

  </sect2>

  <sect2 id="sec-ApplyingGenericTreeModel">
    <title>Utilizaci�n de GenericTreeModel</title>

    <para>La clase <classname>GenericTreeModel</classname> deber�a usarse como �ltimo
recurso. Existen mecanismos muy poderosos en el grupo est�ndar de objetos 
<classname>TreeView</classname> que deber�an ser suficientes para la mayor parte de
aplicaciones. Sin duda que existen aplicaciones que pueden requerir el suo de
<classname>GenericTreeModel</classname> pero se deber�a probar antes a utilizar lo
siguiente:</para>

    <variablelist>
      <varlistentry>
        <term>Funciones de Datos de Celda</term>
        <listitem>
          <para>Como se ilustra en la secci�n <link
linkend="sec-CellDataFunction">Funci�n de Datos de Celda</link>, las funciones de datos de
celda se pueden usar para modificar e incluso generar los datos de una columna
de <classname>TreeView</classname>. Se pueden crear de forma efectiva tantas columnas
con datos generados como se precise. Esto proporciona un gran control sobre la presentaci�n
de los datos a partir de una fuente de datos subyacente.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Modelo de �rbol Filtrado (TreeModelFilter)</term>
        <listitem>
          <para>En PyGTK 2.4, el <classname>TreeModelFilter</classname> que se describe en
la secci�n <link linkend="sec-TreeModelFilter">TreeModelFilter</link>
proporciona un gran nivel de control sobre la visualizaci�n de las columnas y filas de un
<classname>TreeModel</classname> hijo, incluida la presentaci�n de �nicamente las filas
hijas de una fila. Las columnas de datos tambi�n pueden ser generadas de forma similar
al caso de las Funciones de Datos de Celda, pero aqu� el modelo parece ser un
<classname>TreeModel</classname> con el n�mero y tipo de columnas especificado, mientras
que la funci�n de datos de celda deja intactas las columnas del modelo y simplemente modifica
la visualizaci�n en una vista <classname>TreeView</classname>.</para>
        </listitem>
      </varlistentry>
    </variablelist>

    <para>Si se acaba usando un <classname>GenericTreeModel</classname> se debe tener en
cuenta que:</para>

    <itemizedlist>
      <listitem>
        <simpara>la interfaz <classname>TreeModel</classname> completa debe ser creada y
debe funcionar tal como se document�. Hay sutilezas que pueden conducir a errores. Por el
contrario, los <classname>TreeModel</classname>s est�ndar est�n muy revisados.</simpara>
      </listitem>
      <listitem>
        <simpara>la gesti�n de referencias a objetos de Python utilizados por iteradores
<classname>TreeIter</classname> puede ser complicada, especialmente en el caso de programas
que se ejecuten durante mucho tiempo y con gran variedad de visualizaciones.</simpara>
      </listitem>
      <listitem>
        <simpara>es necesaria la adici�n de una interfaz para a�adir, borrar y cambiar los
contenidos de las filas. Hay cierta complicaci�n con la traducci�n de los
<classname>TreeIter</classname> a objetos de Python y a las filas del modelo en esta
interfaz.</simpara>
      </listitem>
      <listitem>
        <simpara>la implementaci�n de las interfaces de ordenaci�n y arrastrar y soltar exigen un
esfuerzo considerable. La aplicaci�n posiblemente necesite implicarse en hacer que estas
interfaces sean completamente funcionales.</simpara>
      </listitem>
    </itemizedlist>

  </sect2>

</sect1>
