<?xml version="1.0" encoding="iso-8859-1" standalone="no"?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" 
"http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">
<!-- ********************************************************************** -->
  <chapter id="ch-GettingStarted">
    <title>Primeros Pasos</title>
    <para>Para empezar nuestra introducci�n a PyGTK, comenzaremos con el
programa m�s simple posible. Este programa (<ulink url="examples/base.py"><command>base.py</command></ulink>) crear� una ventana de 200x200 p�xeles y no es posible salir de �l excepto terminando el proceso desde la consola.</para>

<programlisting>
    1   #!/usr/bin/env python
    2
    3   # example base.py
    4
    5   import pygtk
    6   pygtk.require('2.0')
    7   import gtk
    8
    9   class Base:
   10       def __init__(self):
   11           self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
   12           self.window.show()
   13
   14       def main(self):
   15           gtk.main()
   16
   17   print __name__
   18   if __name__ == "__main__":
   19       base = Base()
   20       base.main()
</programlisting>

    <para>Se puede ejecutar el programa anterior escribiendo en la l�nea de �rdenes:</para>

<programlisting>
  python base.py
</programlisting>

    <para>Si <ulink url="examples/base.py"><command>base.py</command></ulink> 
es hecho ejecutable y se puede encontrar en la variable
<varname>PATH</varname>, es posible ejecutarlo usando:</para>

<programlisting>  base.py</programlisting>

    <para>En este caso, la l�nea 1 pedir� al int�rprete de Python que ejecute 
<ulink url="examples/base.py"><command>base.py</command></ulink>.
Las l�neas 5-6 ayudan a diferenciar entre las distintas versiones de PyGTK que
puedan estar instaladas en el equipo. Estas l�neas indican que se desea
usar la versi�n 2.0 de PyGTK, que comprende todas las versiones de PyGTK con
2 como n�mero principal. Ello impide que el programa utilice versiones
anteriores de PyGTK, en caso de que se encuentren instaladas en el sistema.
Las l�neas 18-20 comprueban si la variable <varname>__name__</varname> es 
<literal>"__main__"</literal>, lo cual indica que el programa est� siendo
ejecutado directamente por python y no est� siendo importado en un int�rprete
Python. En el primer caso el programa crea una nueva instancia de la clase
<classname>Base</classname>
y guarda una referencia a ella en la variable <varname>base</varname>. Despu�s llama la funci�n
 <function>main</function>() para iniciar el bucle de procesamiento de eventos
de GTK.</para>

    <para>Una ventana similar a <xref linkend="basefig"/> deber�a aparecer en
tu pantalla.</para>

    <figure id="basefig">
      <title>Ventana Simple PyGTK</title>
      <mediaobject>
	<imageobject>
	  <imagedata fileref="figures/base.png" format="png" align="center"/>
	</imageobject>
      </mediaobject>
    </figure>

    <para>La primera l�nea permite al programa <ulink
url="examples/base.py"><command>base.py</command></ulink> ser invocado desde
una consola Linux o Unix asumiendo que <command>python</command> se encuentre
en el <varname>PATH</varname>. Esta l�nea aparecer� como primera l�nea en todos los
programas de ejemplo.</para>

    <para>Las l�neas 5-7 importan el m�dulo PyGTK 2 e inicializan el entorno
GTK+. El m�dulo PyGTK define las interfaces Python de las funciones GTK+ que
se usar�n en el programa. Para quienes est�n familiarizados con GTK+ hay que advertir que la 
inicializaci�n incluye la llamada a la funci�n <function>gtk_init</function>().
Tambi�n se configuran algunas cosas por nosotros, tales como el visual por defecto,
el mapa de colores, manejadores de se�ales predeterminados. Asimismo comprueba los
argumentos que se pasan al programa desde la l�nea de comandos, en busca de
alguno entre:</para>

    <itemizedlist spacing="normal">
      <listitem><simpara>--gtk-module</simpara></listitem>
      <listitem><simpara>--g-fatal-warnings</simpara></listitem>
      <listitem><simpara>--gtk-debug</simpara></listitem>
      <listitem><simpara>--gtk-no-debug</simpara></listitem>
      <listitem><simpara>--gdk-debug</simpara></listitem>
      <listitem><simpara>--gdk-no-debug</simpara></listitem>
      <listitem><simpara>--display</simpara></listitem>
      <listitem><simpara>--sync</simpara></listitem>
      <listitem><simpara>--name</simpara></listitem>
      <listitem><simpara>--class</simpara></listitem>
    </itemizedlist>

    <para>En este caso, los borra de la lista de argumentos y deja los no coincidentes
que no reconoce para que el programa lo procese o ignore. El anterior conjunto de
argumentos son los que aceptan de forma est�ndar todos los programas GTK+.</para>

    <para>Las l�neas 9-15 definen una clase de Python llamada
<classname>Base</classname> que define un m�todo de inicializaci�n de instancia
<methodname>__init__</methodname>(). La funci�n <methodname>__init__</methodname>()
crea una ventana de nivel superior (l�nea 11) y ordena a GTK+ que la muestre
(l�nea 12). La <classname>gtk.Window</classname> se crea en la l�nea 11 con el argumento 
<literal>gtk.WINDOW_TOPLEVEL</literal> que indica que se desea una ventana
sometida a las decoraciones y posicionamiento del manejador de ventanas. En
vez de crear una ventana de tama�o 0x0, una ventana sin hijos tiene un tama�o predeterminado
de 200x200 de forma que se pueda manipular.</para>

    <para>Las l�neas 14-15 definen el m�todo <methodname>main</methodname>() que
llama a la funci�n PyGTK <methodname>main</methodname>(), que invoca el bucle
principal de procesamiento de eventos de GTK+ para manejar eventos de rat�n y
de teclado, as� como eventos de ventana.</para>

    <para>Las l�neas 18-20 permiten al programa comenzar autom�ticamente
si es llamado directamente o pasado como argumento al int�rprete de Python.
En estos casos, el nombre de programa que hay en la variable
<varname>__name__</varname> ser� la cadena <literal>"__main__"</literal> y el
c�digo entre las l�neas 18-20 se ejecutar�. Si el programa se carga en
un int�rprete de Python en ejecuci�n, las l�neas 18-20 no ser�n
ejecutadas.</para>

    <para>La l�nea 19 crea una instancia de la clase 
<classname>Base</classname> llamada <parameter>base</parameter>.
Crea una <classname>gtk.Window</classname> y la muestra como resultado.</para>

    <para>La l�nea 20 llama al m�todo <methodname>main</methodname>() de la clase
<classname>Base</classname>, la cual comienza el bucle de procesamiento de 
eventos de GTK+. Cuando el control llega a este punto, GTK+ se dormir� a la espera
de eventos de las X (como pulsaciones de teclas o botones), alarmas, o 
notificaciones de entrada/salida de ficheros. En el ejemplo, sin embargo,
los eventos son ignorados.</para>

<!-- ===================================================================== -->
    <sect1 id="sec-HelloWorld">
      <title>Hola Mundo en PyGTK</title>
      <para>Ahora seguimos con un programa con un control (un bot�n). Es la versi�n PyGTK del cl�sico programa hola mundo (<ulink
url="examples/helloworld.py"><command>helloworld.py</command></ulink>
).</para>
  <programlisting>
    1   #!/usr/bin/env python
    2
    3   # ejemplo helloworld.py
    4
    5   import pygtk
    6   pygtk.require('2.0')
    7   import gtk
    8
    9   class HelloWorld:
   10
   11       # Esta es una funci�n de retrollamada. Se ignoran los argumentos de datos
   12       # en este ejemplo. M�s sobre retrollamadas m�s abajo.
   13       def hello(self, widget, data=None):
   14           print "Hello World"
   15
   16       def delete_event(self, widget, event, data=None):
   17           # Si se devuelve FALSE en el gestor de la se�al "delete_event",
   18           # GTK emitir� la se�al "destroy". La devoluci�n de TRUE significa
   19           # que no se desea la destrucci�n de la ventana.
   20           # Esto sirve para presentar di�logos como: '�Est� seguro de que desea salir?'
   21           # 
   22           print "delete event occurred"
   23
   24           # Si se cambia FALSE a TRUE la ventana principal no se
   25           # destruir� con "delete_event".
   26           return gtk.FALSE
   27
   28       # Otra retrollamada
   29       def destroy(self, widget, data=None):
   30           gtk.main_quit()
   31
   32       def __init__(self):
   33           # se crea una ventana nueva
   34           self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
   35
   36           # Cuando se env�a a una ventana la se�al "delete_event" (esto lo hace
   37           # generalmente el gestor de ventanas, usualmente con "cerrar", o con el icono
   38           # de la ventana de t�tulo), pedimos que llame la funci�n delete_event ()
   39           # definida arriba. Los datos pasados a la retrollamada son
   40           # NULL y se ignoran en la funci�n de retrollamada.
   41           self.window.connect("delete_event", self.delete_event)
   42
   43           # Conectamos el evento "destroy" a un manejador de se�al.
   44           # Este evento sucede cuando llamamos gtk_widget_destroy() para la ventana,
   45           # o si devolvemos FALSE en la retrollamada "delete_event".
   46           self.window.connect("destroy", self.destroy)
   47
   48           # Establece el grosor del borde de la ventana.
   49           self.window.set_border_width(10)
   50
   51           # Crea un nuevo bot�n con la etiqueta "Hello World".
   52           self.button = gtk.Button("Hello World")
   53
   54           # Cuando el bot�n recibe la se�al "clicked", llamar� la
   55           # funci�n hello() a la que pasa None como argumento.  La funci�n hello()
   56           # se define m�s arriba.
   57           self.button.connect("clicked", self.hello, None)
   58
   59           # Esto causar� la destrucci�n de la ventana al llamar a
   60           # gtk_widget_destroy(window) cuando se produzca "clicked".  De nuevo,
   61           # la se�al podr�a venir de aqu� o del gestor de ventanas.
   62           self.button.connect_object("clicked", gtk.Widget.destroy, self.window)
   63
   64           # Esto empaqueta el bot�n en la ventana (un contenedor de GTK+).
   65           self.window.add(self.button)
   66
   67           # El paso final es mostrar el control reci�n creado.
   68           self.button.show()
   69
   70           # y la ventana
   71           self.window.show()
   72
   73       def main(self):
   74           # Todas las aplicaciones de PyGTK deben tener una llamada a gtk.main(). Aqu� se deja
   75           # el control y se espera que suceda un evento (como un evento de teclado o rat�n).
   76           gtk.main()
   77
   78   # Si el programa se ejecuta directamente o se pasa como argumento al int�rprete
   79   # de Python, entonces se crea una instancia de HelloWorld y se muestra
   80   if __name__ == "__main__":
   81       hello = HelloWorld()
   82       hello.main()
</programlisting>
      <para><xref linkend="helloworldfig"/> muestra la ventana creada por 
<ulink url="examples/helloworld.py"><command>helloworld.py</command></ulink>.
</para>
      <figure id="helloworldfig">
	<title>Programa de ejemplo: Hola Mundo</title>
	<mediaobject>
	  <imageobject>
	    <imagedata fileref="figures/helloworld.png" format="png" align="center"/>
	  </imageobject>
	</mediaobject>
      </figure>
      <para> Las variables y funciones que se definen en el m�dulo PyGTK se
llaman de la forma <literal>gtk.*</literal>. Por ejemplo, el programa <ulink
url="examples/helloworld.py"><command>helloworld.py</command></ulink> usa:
</para>
<programlisting>
  gtk.FALSE
  gtk.mainquit()
  gtk.Window()
  gtk.Button()
</programlisting>
      <para>del m�dulo PyGTK. En futuras secciones no se especificar� el prefijo
del m�dulo gtk, pero se dar� por asumido. Naturalmente, los programas de ejemplo
usar�n los prefijos del m�dulo.</para>
    </sect1>

<!-- =================================================================== -->
    <sect1 id="sec-TheoryOfSignalsAndCallbacks">
      <title>Teor�a de Se�ales y Retrollamadas</title>
      <note>
	<para>En la versi�n 2.0 de GTK+, el sistema de se�ales se ha movido de
GTK+ a GLib. No entraremos en detalles sobre las extensiones que GLib 2.0 tiene
en relaci�n con el sistema de se�ales de GTK 1.2. Las diferecias no deber�an
notarse en el uso de PyGTK.</para>
      </note>
      <para>Antes de entrar en detalle en <ulink
url="examples/helloworld.py"><command>helloworld.py</command></ulink>,
discutiremos las se�ales y las retrollamadas. GTK+ es una biblioteca orientada a
eventos, lo que significa que se dormir� en la funci�n
 <methodname>gtk.main</methodname>() hasta que un evento ocurra y el control pase
a la funci�n apropiada.</para>

      <para>Esta delegaci�n del control se realiza usando la idea de
"se�ales". (N�tese que estas se�ales no son las mismas que las se�ales de los
sistemas Unix, y no se implementan usando �stas, aunque la terminolog�a es casi
id�ntica) Cuando ocurre un evento, como cuando presionamos un bot�n del rat�n,
la se�al apropiada se "emite" por el el control que fu� presionado. As� es
c�mo GTK+ hace la mayor�a de su trabajo �til. Hay se�ales que todos los
controles heredan, como "destroy", y hay se�ales que son espec�ficas de cada
control, como "toggled" en el caso de un bot�n de activaci�n.</para>

      <para>Para hacer que un bot�n realice una acci�n, debemos configurar
un manejador de se�ales que capture estas se�ales y llame a la funci�n 
apropiada. Esto se hace usando un m�todo de <classname>gtk.Widget</classname> 
(heredado de la clase <classname>GObject</classname>) como por ejemplo:</para>

<programlisting>
  handler_id = object.connect(<parameter role="keyword">name</parameter>, <parameter role="keyword">func</parameter>, <parameter role="keyword">func_data</parameter>)
</programlisting>

      <para>donde <parameter>object</parameter> es la instancia de
<classname>gtk.Widget</classname> (un control)
que estar� emitiendo la se�al, y el primer argumento <parameter>name</parameter>
es una cadena que contiene el nombre de la se�al que se desea capturar. El
segundo argumento, <parameter>func</parameter>, es la funci�n que se quiere
llamar cuando se produce el evento. El tercer argumento, 
<parameter>func_data</parameter>, son los datos que se desean pasar a la funci�n 
<parameter>func</parameter>. El
m�todo devuelve un <returnvalue>handler_id</returnvalue> que se puede usar para
desconectar o bloquear el uso del manejador.</para>

      <para>La funci�n especificada en el tercer argumento se llama "funci�n
de retrollamada", y generalmente tiene la forma:</para>

<programlisting>
  def callback_func(<parameter role="keyword">widget</parameter>, <parameter role="keyword">callback_data</parameter>):
</programlisting>

      <para>donde el primer argumento ser� una referencia al
<parameter>widget</parameter> (control) que emiti� la se�al, y el segundo
(<parameter>callback_data</parameter>) una referencia a los datos dados como 
�ltimo argumento en el m�todo <methodname>connect</methodname>() mostrado antes.
</para>

      <para>Si la funci�n de retrollamada es un m�todo de un objeto entonces
tendr� la forma general siguiente:</para>

<programlisting>
  def callback_meth(<parameter role="keyword">self</parameter>, <parameter role="keyword">widget</parameter>, <parameter role="keyword">callback_data</parameter>):
</programlisting>

      <para>donde <parameter>self</parameter> es la instancia del objeto que
invoca este m�todo. Esta es la forma usada en el programa de ejemplo
<ulink url="examples/helloworld.py"><command>helloworld.py</command></ulink>.
</para>

      <note><para>La forma anterior de declaraci�n de una funci�n de 
retrollamada a se�ales es s�lo una gu�a general, ya que las se�ales espec�ficas de los distintos
controles generan diferentes par�metros de llamada.</para></note>

      <para>Otra llamada que se usa en el ejemplo <ulink
url="examples/helloworld.py"><command>helloworld.py</command></ulink>
es:</para>

<programlisting>
  handler_id = object.connect_object(<parameter role="keyword">name</parameter>, <parameter role="keyword">func</parameter>, <parameter role="keyword">slot_object</parameter>)
</programlisting>
      <para><methodname>connect_object</methodname>() es id�ntica a
<methodname>connect</methodname>(), exceptuando que una funci�n de retrollamada s�lo
usa un argumento, y un m�todo de retrollamada, dos argumentos:</para>

<programlisting>
  def callback_func(<parameter role="keyword">object</parameter>)
  def callback_meth(<parameter role="keyword">self</parameter>, <parameter role="keyword">object</parameter>)
</programlisting>

      <para>donde <parameter>object</parameter> normalmente es un control.
<methodname>connect_object</methodname>() permite usar los m�todos de controles
PyGTK qu� solo admiten un argumento (<parameter>self</parameter>) como
manejadores de se�ales.</para>
    </sect1>

<!-- =================================================================== -->
    <sect1 id="sec-Events">
      <title>Eventos</title>
      <para>Adem�s del mecanismo de se�ales descrito anteriormente, hay un
conjunto de eventos que reflejan el mecanismo de eventos de X. Las
retrollamadas tambi�n se pueden conectar a estos eventos. Estos eventos son:
</para>

<programlisting>
  event
  button_press_event
  button_release_event
  scroll_event
  motion_notify_event
  delete_event
  destroy_event
  expose_event
  key_press_event
  key_release_event
  enter_notify_event
  leave_notify_event
  configure_event
  focus_in_event
  focus_out_event
  map_event
  unmap_event
  property_notify_event
  selection_clear_event
  selection_request_event
  selection_notify_event
  proximity_in_event
  proximity_out_event
  visibility_notify_event
  client_event
  no_expose_event
  window_state_event
</programlisting>

      <para>Para conectar una funci�n de retrollamada a uno de estos eventos
se usa el m�todo <methodname>connect</methodname>(), como se ha dicho
anteriormente, usando uno de los nombres de eventos anteriores en el par�metro
<parameter>name</parameter>. La funci�n (o m�todo) de retrollamada para eventos
es ligeramente diferente de la usada para se�ales:</para>

<programlisting>
  def callback_func(<parameter role="keyword">widget</parameter>, <parameter role="keyword">event</parameter>, <parameter role="keyword">callback_data</parameter>):

  def callback_meth(<parameter role="keyword">self</parameter>, <parameter role="keyword">widget</parameter>, <parameter role="keyword">event</parameter>, <parameter role="keyword">callback_data</parameter>):
</programlisting>

      <para><classname>gdk.Event</classname> es un tipo de objetos Python cuyos
atributos de tipo indicar�n cu�l de los eventos anteriores ha ocurrido. Los
otros atributos del evento depender�n del tipo de evento. Los valores posibles
para los tipos son:</para>
 
<programlisting>
  NOTHING
  DELETE
  DESTROY
  EXPOSE
  MOTION_NOTIFY
  BUTTON_PRESS
  _2BUTTON_PRESS
  _3BUTTON_PRESS
  BUTTON_RELEASE
  KEY_PRESS
  KEY_RELEASE
  ENTER_NOTIFY
  LEAVE_NOTIFY
  FOCUS_CHANGE
  CONFIGURE
  MAP
  UNMAP
  PROPERTY_NOTIFY
  SELECTION_CLEAR
  SELECTION_REQUEST
  SELECTION_NOTIFY
  PROXIMITY_IN
  PROXIMITY_OUT
  DRAG_ENTER
  DRAG_LEAVE
  DRAG_MOTION
  DRAG_STATUS
  DROP_START
  DROP_FINISHED
  CLIENT_EVENT
  VISIBILITY_NOTIFY
  NO_EXPOSE
  SCROLL
  WINDOW_STATE
  SETTING
</programlisting>

      <para>Para acceder a estos valores se a�ade el prefijo <literal>gtk.gdk.</literal> al tipo
de evento. Por ejemplo, <literal>gtk.gdk.DRAG_ENTER</literal>.</para>

      <para>Por tanto, para conectar una funci�n de retrollamada a uno de
estos eventos se usar�a algo como:</para>

<programlisting>
  button.connect(<parameter role="keyword">"button_press_event"</parameter>, <parameter role="keyword">button_press_callback</parameter>)
</programlisting>

      <para>Esto asume que <parameter>button</parameter> es un control
 <classname>gtk.Button</classname>. Entonces, cuando el rat�n est� sobre el
bot�n y se pulse un bot�n del rat�n, se llamar� a la funci�n
<parameter>button_press_callback</parameter>. Esta funci�n se puede definir
as�:
</para>

<programlisting>
  def button_press_callback(<parameter role="keyword">widget</parameter>, <parameter role="keyword">event</parameter>, <parameter role="keyword">data</parameter>):
</programlisting>

      <para>El valor que devuelve esta funci�n indica si el evento debe ser
propagado por el sistema de manejo de eventos GTK+. Devolviendo
<literal>gtk.TRUE</literal> indicamos que el evento ha sido procesado, y que
no debe ser propagado. Devolviendo <literal>gtk.FALSE</literal> se continua
el procesamiento normal del evento. Es aconsejable consultar la secci�n
<link linkend="ch-AdvancedEventAndSignalHandling">Procesamiento Avanzado de 
Eventos y Se�ales</link> para obtener m�s detalles sobre el sistema de propagaci�n.
</para>

      <para>Las APIs de selecci�n y arrastrar y soltar de GDK tambi�n emiten
unos cuantos eventos que se reflejan en GTK+ por medio de se�ales. Consulta
<link linkend="sec-SignalsOnSourceWidget">Se�ales en el Control de Or�gen
</link> y <link linkend="sec-SignalsOnDestinationWidget">Se�ales en el Control
de Destino</link> para obtener m�s detalles sobre la sintaxis de las funciones de
retrollamada para estas se�ales:</para>

<programlisting>
  selection_received
  selection_get
  drag_begin_event
  drag_end_event
  drag_data_delete
  drag_motion
  drag_drop
  drag_data_get
  drag_data_received
</programlisting>

    </sect1>

<!-- =================================================================== -->
    <sect1 id="sec-SteppingThroughHelloWorld">
      <title>Hola Mundo Paso a Paso</title>
      <para>Ahora que ya conocemos la teor�a general, vamos
a aclarar el programa de ejemplo <ulink
url="examples/helloworld.py"><command>helloworld.py</command></ulink> paso a
paso.</para>

      <para>Las l�neas 9-76 definen la clase <classname>HelloWorld</classname>,
que contiene todas las retrollamadas como m�todos de objeto y el m�todo de
inicializaci�n de objetos. Examinemos los m�todos de retrollamada:</para>

      <para>Las l�neas 13-14 definen el m�todo de retrollamada
<methodname>hello</methodname>() que ser� llamado al pulsar el bot�n.
Cuando se llama a este m�todo, se imprime "Hello World" en la consola. En el ejemplo ignoramos los par�metros de la instancia del objeto, el control y los
datos, pero la mayor�a de las retrollamadas los usan. El par�metro
<parameter>data</parameter> se define con un valor predeterminado igual a
<literal>None</literal> porque PyGTK no pasar� ning�n valor para los datos si
no son incluidos en la llamada a <methodname>connect</methodname>(); esto
producir�a un error ya que la retrollamada espera tres argumentos y �nicamente recibe
dos. La definici�n de un valor por defecto <literal>None</literal> permite llamar a
la retrollamada con dos o tres par�metros sin ning�n error. En este caso el par�metro
de datos podr�a haberse omitido, ya que el m�todo <methodname>hello</methodname>()
siempre ser� llamado con s�lo dos par�metros (nunca se usan los datos de usuario). En el
siguiente ejemplo usaremos el argumento de datos para saber qu� bot�n fue
pulsado.</para>

<programlisting>
  def hello(self, widget, data=None):
      print "Hello World"
</programlisting>

      <para>La siguiente retrollamada (l�neas 16-26) es un poco especial. El
evento "delete_event" se produce cuando el manejador de ventanas manda este
evento al programa. Tenemos varias posibilidades en cuanto a qu� hacer con
estos eventos. Podemos ignorarlos, realizar algun tipo de respuesta, o
simplemente cerrar el programa.</para>

      <para>El valor que se devuelva en esta retrollamada le permite a GTK+ saber
qu� acci�n realizar. Si devolvemos <literal>TRUE</literal> hacemos saber que
no queremos que se emita la se�al "destroy", y as� nuestra aplicaci�n sigue
ejecut�ndose. Si devolvemos <literal>FALSE</literal> pedimos que se emita la
se�al "destroy", que a su vez llamar� a nuestro manejador de la se�al
"destroy". N�tese que se han quitado los comentarios para mayor claridad.
</para>

<programlisting>
  def delete_event(widget, event, data=None):
      print "delete event occurred"
      return gtk.FALSE
</programlisting>

      <para>El m�todo de retrollamada <methodname>destroy</methodname>()
(l�neas 29-30) hace que el programa termine mediante la lllamada a
<function>gtk.main_quit</function>(). Esta funci�n indica a GTK que debe
salir de la funci�n <function>gtk.main</function>() cuando el control le sea
transferido.</para>

<programlisting>
  def destroy(widget, data=None):
      gtk.main_quit()
</programlisting>

      <para>Las l�neas 32-71 definen el m�todo de inicializaci�n de instancia
<methodname>__init__</methodname>() del objeto
<classname>HelloWorld</classname>, el cual crea la ventana y los controles que
se usan en el programa.</para>

      <para>La l�nea 34 crea una nueva ventana, pero no se muestra
directamente hasta que comunicamos a GTK+ que lo haga, casi al final del
programa. La referencia a la ventana se guarda en un atributo de instancia
(self.window) para poder acceder a ella despu�s.</para>

<programlisting>
    self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
</programlisting>

      <para>Las l�neas 41 y 46 ilustran dos ejemplos de c�mo conectar un
manejador de se�al a un objeto, en este caso a <varname>window</varname>.
Aqu� se captura el evento "delete_event" y la se�al "destroy". El primero se
emite cuando cerramos la ventana a trav�s del manejador de ventanas o cuando
usamos la llamada al m�todo <methodname>destroy</methodname>() de
<classname>gtk.Widget</classname>. La segunda se emite cuando, en el manejador
de "delete_event", devolvemos <literal>FALSE</literal>.</para>

<programlisting>
    self.window.connect("delete_event", self.delete_event)
    self.window.connect("destroy", self.destroy)
</programlisting>

      <para>La l�nea 49 establece un atributo de un objeto contenedor (en este
caso <varname>window</varname>) para que tenga un �rea vac�a de 10 p�xeles de
ancho a su alrededor en donde no se sit�e ning�n control. Hay otras funciones
similares que se tratar�n en la secci�n <link
linkend="ch-SettingWidgetAttributes">Establecimiento de Atributos de Controles</link>
</para>

<programlisting>
    self.window.set_border_width(10)
</programlisting>

      <para>La l�nea 52 crea un nuevo bot�n y guarda una referencia a �l en
<varname>self.button</varname>. El bot�n tendr� la etiqueta "Hello World"
cuando se muestre.</para>

<programlisting>
    self.button = gtk.Button("Hello World")
</programlisting>

      <para>En la l�nea 57 conectamos un manejador de se�al al bot�n, de forma que,
cuando emita la se�al "clicked", se llame a nuestro manejador de retrollamada
<methodname>hello</methodname>(). No pasamos ning�n dato a
<methodname>hello</methodname>() as� que simplemente se entrega
<literal>None</literal> como dato. Obviamente, la se�al "clicked" se emite
al hacer clic en el bot�n con el cursor del rat�n. El valor del par�metro
de los datos <literal>None</literal> no es imprescindible y podr�a omitirse.
La retrollamada se llamar� con un par�metro menos.</para>

<programlisting>
    self.button.connect("clicked", self.hello, None)
</programlisting>

      <para>Tambi�n vamos a usar este bot�n para salir del programa. La
l�nea 62 muestra c�mo la se�al "destroy" puede venir del manejador de ventanas,
o de nuestro programa. Al hacer clic en el bot�n, al igual que antes,
se llama primero a la retrollamada <methodname>hello</methodname>(), y despu�s
a la siguiente en el orden en el que han sido configuradas. Se pueden tener todas las
retrollamadas que sean necesarias, y se ejecutar�n en el orden en el que se hayan
conectado.</para>

      <para>Como queremos usar el m�todo <methodname>destroy</methodname>() de
la clase <classname>gtk.Widget</classname> que acepta un argumento (el control
que se va a destruir - en este caso <varname>window</varname>), utilizamos el
m�todo <methodname>connect_object</methodname>() y le pasamos la referencia a
la ventana. El m�todo <methodname>connect_object</methodname>() organiza el
primer argumento de la retrollamada para que sea <varname>window</varname> en
vez del bot�n.</para>

      <para>Cuando se llama el m�todo <methodname>destroy</methodname>() de la
clase <classname>gtk.Widget</classname> se emite la se�al
"destroy" desde la ventana, lo que a su vez provocar� la llamada al m�todo 
<methodname>destroy</methodname>() de la clase
<classname>HelloWorld</classname> que termina el programa.</para>

<programlisting>
    self.button.connect_object("clicked", gtk.Widget.destroy, self.window)
</programlisting>

      <para>La l�nea 65 es una llamada de colocaci�n, que se explicar� en
profundidad m�s tarde, en <link linkend="ch-PackingWidgets">Colocaci�n de Controles
</link>, aunque es bastante f�cil de entender. Simplemente indica a GTK+ que
el bot�n debe situarse en la ventana en donde se va a mostrar. Ha de tenerse en cuenta que
un contenedor GTK+ �nicamente puede contener un control. Otros controles, descritos
m�s adelante, est�n dise�ados para posicionar varios controles de otras maneras.</para>

<programlisting>
    self.window.add(self.button)
</programlisting>

      <para>Ahora lo tenemos todo configurado como queremos. Con todos los 
manejadores de se�ales, y el bot�n situado en la ventana donde deber�a estar,
pedimos a GTK (l�neas 66 y 69) que muestre los controles en pantalla.
El control de ventana se muestra en �ltimo lugar, para que la ventana entera
aparezca de una vez y no primero la ventana y luego el bot�n dentro
de ella dibuj�ndose. Sin embargo, con un ejemplo tan simple, ser�a dif�cil apreciar la
diferencia.</para>

<programlisting>
    self.button.show()

    self.window.show()
</programlisting>

      <para>Las l�neas 73-75 definen el m�todo <function>main</function>() que
llama a la funci�n <function>gtk.main</function>()</para>

<programlisting>
    def main(self):
        gtk.main()
</programlisting>

      <para>Las l�neas 80-82 permiten al programa ejecutarse autom�ticamente
si es llamado directamente o como argumento del int�rprete de python. La 
l�nea 81 crea una instancia de la clase <classname>HelloWorld</classname> y
guarda una referencia a ella en la variable <varname>hello</varname>. La l�nea
82 llama al m�todo <function>main</function>() de la clase
<classname>HelloWorld</classname> para empezar el bucle de procesamiento de
eventos GTK.</para>

<programlisting>
    if __name__ == "__main__":
        hello = HelloWorld()
        hello.main()
</programlisting>

      <para>Ahora, cuando hagamos clic con el bot�n del rat�n en el bot�n GTK,
el control emitir� una se�al "clicked". Para poder usar esta informaci�n,
nuestro programa configura un manejador de se�al que capture esta se�al,
la cual llama a la funci�n que decidamos. En nuestro ejemplo, cuando se pulsa el bot�n
que hemos creado, se llama el m�todo
<methodname>hello</methodname>() con un argumento <varname>None</varname>, y
despu�s se llama el siguiente manejador para esta se�al. El siguiente manejador
llama a la funci�n <methodname>destroy</methodname>() del control con la
ventana como su argumento y de esta manera causa que la ventana emita la
se�al "destroy", que es capturada y llama al m�todo
<methodname>destroy</methodname>() de la clase
<classname>HelloWorld</classname></para>

      <para>Otra funci�n de los eventos es usar el manejador de ventanas para
eliminar la ventana, lo que causar� que se emita "delete_event". Esto llamar� a
nuestro manejador de "delete_event". Si devolvemos <literal>TRUE</literal>
aqu�, la ventana se quedar� como si nada hubiera pasado. Devolviendo 
<literal>FALSE</literal> har� que GTK+ emita la se�al "destroy", que llama a 
la retrollamada "destroy" de la clase <classname>HelloWorld</classname>
cerrando GTK+.</para>
    </sect1>
  </chapter>
