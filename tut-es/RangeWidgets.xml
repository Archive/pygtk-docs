<?xml version="1.0" encoding="iso-8859-1" standalone="no"?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" 
"http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">

<!-- ********************************************************************** -->
  <chapter id="ch-RangeWidgets">
    <title>Controles de Rango</title>

    <para>La categor�a de los controles de rango incluye el ub�cuo control
de barra de desplazamiento y el menos com�n control de escala. Aunque estos
dos tipos de controles se usan generalmente para prop�sitos diferentes, son
bastante similares en funci�n e implementaci�n. Todos los controles de rango
comparten un conjunto de elementos gr�ficos, cada uno de los cuales tiene su
propia ventana X y recibe eventos. Todos ellos contienen una gu�a o canal
y un deslizador. Arrastrar el deslizador con el puntero del rat�n hace que se
mueva hacia adelante y hacia atr�s en el canal, mientras que
haciendo clic en el canal avanza el deslizador hacia la localizaci�n
del clic, ya sea completamente, o con una cantidad designada, dependiendo
del bot�n del rat�n que se use.</para>

    <para>Como se mencion� en <link linkend="ch-Adjustments">Adjustments</link>
m�s arriba, todos los controles de rango est�n asociados con un objeto ajuste,
a partir del cual se calcula la longitud del deslizador y su posici�n con 
respecto al canal. Cuando el usuario manipula el deslizador, el
control de rango cambiar� el valor del ajuste.</para>

<!-- ===================================================================== -->
    <sect1 id="sec-ScrollbarWidgets">
      <title>Barras de Desplazamiento</title>

      <para>Estas son las barras de desplazamiento est�ndar. Deber�an
usarse �nicamente para desplazar otro control, tal como una lista, una caja
de texto, o una vista (viewport), aunque, generalmente, es m�s f�cil de usar la ventana
de desplazamiento en la mayor�a de los casos. Para otros prop�sitos,
se deber�an usar los controles de escala, ya que son m�s agradables y tienen
m�s funciones.</para>

      <para>Existen tipos separados para las barras de desplazamiento
horizontales y verticales. No hay demasiado que decir sobre ellos. Los puedes
crear con los siguientes m�todos:</para>

      <programlisting>
  hscrollbar = gtk.HSscrollbar(<parameter role="keyword">adjustment</parameter>=None)

  vscrollbar = gtk.VSscrollbar(<parameter role="keyword">adjustment</parameter>=None)
</programlisting>

      <para>y eso es todo. El argumento <parameter>adjustment</parameter>
puede ser, o bien una referencia a un <link linkend="ch-Adjustments">
<classname>Adjustment</classname></link> existente,
o bien nada, en cuyo caso se crear� uno. Especificar nada puede ser �til en el
caso en el que se quiera pasar el ajuste reci�n creado al constructor de
otro control que lo configurar� por uno, tal como podr�a ocurrir con una caja de texto.
</para>
    </sect1>

<!-- ===================================================================== -->
    <sect1 id="sec-ScaleWidgets">
      <title>Controles de Escala</title>

      <para>Los controles <classname>Scale</classname> (Escala) se usan para
permitir al usuario seleccionar y manipular visualmente un valor dentro de
un rango espec�fico. Se puede usar un control de escala, por ejemplo,
para ajustar el nivel de zoom en una previsualizaci�n de una imagen, o para
controlar el brillo de un color, o para especificar el n�mero de minutos
de inactividad antes de que el protector de pantalla se active.</para>

      <sect2>
	<title>Creaci�n de un Control de Escala</title>

	<para>Al igual que con las barras de desplazamiento, hay controles
separados para los controles de escala horizontales y verticales. (La mayor�a
de los programadres parecen usar los controles de escala horizontales.) Ya
que esencialmente funcionan de la misma manera, no hay necesidad de tratarlos
por separado aqu�. Los siguientes m�todos crean controles de escala verticales
y horizontales, respectivamente:</para>

	<programlisting>
  vscale = gtk.VScale(<parameter role="keyword">adjustment</parameter>=None)

  hscale = gtk.HScale(<parameter role="keyword">adjustment</parameter>=None)
</programlisting>

	<para>El argumento <parameter>adjustment</parameter> puede ser bien un
ajuste que ya haya sido creado con <function>gtk.Adjustment</function>(), o bien
nada, en cuyo caso se crea un <link linkend="ch-Adjustments">
<classname>Adjustment</classname></link> an�nimo con todos sus valores puestos
a 0.0 (lo cual no es demasiado �til). Para evitar confusiones, 
probablemente sea mejor crear un ajuste con un valor
<varname>page_size</varname> de 0.0 para que su valor <varname>upper</varname>
realmente corresponda con el valor m�s alto que el usuario puede seleccionar.
(Si esto resulta confuso, la secci�n sobre
<link linkend="ch-Adjustments">Ajustes</link> da una explicaci�n detallada
sobre qu� hacen exactamente los ajustes y c�mo crearlos y manipularlos.)</para>
      </sect2>

      <sect2>
	<title>M�todos y Se�ales (bueno, al menos m�todos)</title>

	<para>Los controles de escala pueden visualizar su valor como un
n�mero al lado del canal. El comportamiento por defecto es mostrar el valor,
pero esto se puede cambiar con el m�todo:</para>

	<programlisting>
  scale.set_draw_value(<parameter role="keyword">draw_value</parameter>)
</programlisting>

	<para>Como habr�s imaginado, <parameter>draw_value</parameter> (representar
valor) es o <literal>TRUE</literal> o <literal>FALSE</literal>, con consecuencias predecibles
para cualquiera de los dos.</para>

	<para>El valor que muestra un control de escala se redondea a un
valor decimal por defecto, tal y como se hace con el campo valor en su <link
linkend="ch-Adjustments"><classname>Adjustment</classname></link> (Ajuste). 
Se puede cambiar esto con:</para>

	<programlisting>
  scale.set_digits(<parameter role="keyword">digits</parameter>)
</programlisting>

	<para>donde <parameter>digits</parameter> (d�gitos) es el n�mero de posiciones
decimales que se representar�n. Se puede poner el n�mero que se desee, pero no
se representar�n en pantalla m�s de 13 d�gitos decimales.</para>

	<para>Finalmente, el valor se puede mostrar en diferentes posiciones
relativas al canal:</para>

	<programlisting>
  scale.set_value_pos(<parameter role="keyword">pos</parameter>)
</programlisting>

	<para>El argumento <parameter>pos</parameter> (posici�n) puede tomar uno de los
siguientes valores:</para>

	<programlisting>
  POS_LEFT
  POS_RIGHT
  POS_TOP
  POS_BOTTOM
</programlisting>

	<para>Si pones el valor en un "lado" del canal (por ejemplo, en la
parte de arriba o de abajo de un control de escala horizontal), entonces seguir�
al deslizador en su desplazamiento hacia arriba y hacia abajo del canal.</para>
      </sect2>
    </sect1>

<!-- ===================================================================== -->
    <sect1 id="sec-CommonRangeMethods">
      <title>M�todos Comunes de los Rangos</title>

      <para>La clase <classname>Range</classname> es bastante complicada
internamente, pero, como todas las clases base de los controles, la mayor�a
de su complejidad solo resulta de inter�s si se quiere trastear con ellos. Adem�s,
la mayor�a de los m�todos y se�ales que define s�lo son �tiles al escribir
controles derivados. Hay, en cualquier caso, unos cuantos m�todos �tiles
que funcionar�n con todos los controles de rango.</para>

      <sect2>
	<title>Establecimiento de la Pol�tica de Actualizaci�n</title>

	<para>La "pol�tica de actualizaci�n" de un control de rango define en
qu� puntos de la interacci�n con el usuario se cambiar� el campo de valor de
su <link linkend="ch-Adjustments"><classname>Adjustment</classname></link> y
emitir� la se�al "value_changed" en este <link
linkend="ch-Adjustments"><classname>Adjustment</classname></link>. 
Las pol�ticas de actualizaci�n son:</para>

	<variablelist>
	  <varlistentry>
	    <term> UPDATE_CONTINUOUS</term>
	    <listitem>
	      <para>Es es el valor predeterminado. La se�al "value_changed" se
emite cont�nuamente, por ejemplo, cada vez que el deslizador se mueve, incluso
en las cantidades mas min�sculas.</para>
	    </listitem>
	  </varlistentry>

	  <varlistentry>
	    <term>UPDATE_DISCONTINUOUS</term>
	    <listitem>
	      <para>La se�al "value_changed" s�lo se mite una vez que el
deslizador ha parado de moverse y el usuario ha soltado el bot�n del rat�n.</para>
	    </listitem>
	  </varlistentry>

	  <varlistentry>
	    <term>UPDATE_DELAYED</term>
	    <listitem>
	      <para>La se�al "value_changed" se emite cuando el usuario suelta
el bot�n del rat�n, o si el deslizador deja de moverse durante un corto
per�odo de tiempo.</para>
	    </listitem>
	  </varlistentry>
	</variablelist>

	<para>La pol�tica de actualizaci�n de un control de rango puede
cambiarse con este m�todo:</para>

	<programlisting>
  range.set_update_policy(<parameter role="keyword">policy</parameter>)
</programlisting>

      </sect2>

      <sect2>
	<title>Obtenci�n y Cambio de Ajustes</title>

	<para>La obtenci�n y cambio del ajuste de un control de rango se puede hacer
sobre la marcha, como era predecible, con:</para>

	<programlisting>
  adjustment = range.get_adjustment()

  range.set_adjustment(<parameter role="keyword">adjustment</parameter>)
</programlisting>

	<para>El m�todo <methodname>get_adjustment</methodname>() devuelve una
referencia al <parameter>adjustment</parameter> que est� conectado al rango.</para>

	<para>El m�todo <methodname>set_adjustment</methodname>() no hace
absolutamente nada si se le pasa el <parameter>adjustment</parameter> que el
<parameter>range</parameter> ya est� utilizando, independientemente de que se
le hayan cambiado alguno de sus campos o no. Si se le pasa un nuevo <link
linkend="ch-Adjustments"><classname>Adjustment</classname></link>, se perder�
la referencia al antiguo si exist�a (posiblemente se destruir�), se conectar�n
las se�ales apropiadas al nuevo, y se recalcular� el tama�o y/o posici�n del
deslizador y se repintar� si es necesario. Como se mencion� en la secci�n
de ajustes, si se desea reutilizar el mismo <link
linkend="ch-Adjustments"><classname>Adjustment</classname></link>, cuando
se modifiquen sus valores directamente se debe emitir la se�al "changed" desde �l,
como por ejemplo:</para>

	<programlisting>
  adjustment.emit("changed")
</programlisting>

      </sect2>
    </sect1>

<!-- ===================================================================== -->
    <sect1 id="sec-KeyAndMouseBindings">
      <title>Atajos de Teclas y Rat�n</title>

      <para>Todos los controles de rango de GTK reaccionan a clics de rat�n
m�s o menos de la misma forma. Haciendo clic con el
<mousebutton>bot�n-1</mousebutton> en el canal hace que el valor
<parameter>page_increment</parameter> del ajuste se a�ada o se reste a su
<parameter>value</parameter>, y que el deslizador se mueva de forma acorde.
Haciendo clic con el <mousebutton>bot�n-2</mousebutton> en el canal har� que
el deslizador salte al punto donde se ha hecho clic. Haciendo clic con
cualquier bot�n en las flechas de una barra de desplazamiento se har� que el valor
<parameter>value</parameter> de su ajuste cambie de una vez tanto como diga su
propiedad <parameter>step_increment</parameter>.</para>

      <para>Las barras de desplazamiento no pueden recibir el foco, por lo que
no tienen atajos de teclado. Los atajos de teclado de otros controles de rango
(que por supuesto s�lo est�n ativos cuando el control tiene el foco) no se
diferencian entre controles de rango horizontales y verticales.</para>

      <para>Todos los controles de rango pueden manejarse con las teclas de
<keysym>flecha izquierda</keysym>, <keysym>flecha derecha</keysym>,
<keysym>flecha arriba</keysym> y <keysym>flecha abajo</keysym> , as� como con
las teclas <keycap>P�gina Anterior</keycap> y <keycap>P�gina Siguiente</keycap>. Las flechas mueven el deslizador en cantidades iguales a
<parameter>step_increment</parameter>, mientras que
<keycap>P�gina Anterior</keycap> y <keycap>P�gina Siguiente</keycap> lo mueven
en cantidades de <parameter>page_increment</parameter>.</para>

      <para>El usuario tambi�n puede mover el deslizador directamente a un
extremo u otro del canal usando el teclado. Esto se hace con las teclas
<keycap>Inicio</keycap> y <keycap>Fin</keycap> .</para>
    </sect1>

<!-- ===================================================================== -->
    <sect1 id="sec-RangeWidgetEample">
      <title>Ejemplo de Control de Rango</title>

      <para>El programa de ejemplo (<ulink
url="examples/rangewidgets.py"><command>rangewidgets.py</command></ulink>)
muestra una ventana con tres
controles de rango todos conectados al mismo ajuste, y un par de controles
para modificar algunos de los par�metros mencionados m�s arriba y en la secci�n
de ajustes, por lo que se puede ver c�mo afectan a la manera en la que estos
controles se comportan para el usuario. La figura
<xref linkend="rangefig"/> muestra el resultado de ejecutar el programa:</para>
 
      <figure id="rangefig">
	<title>Ejemplo de Controles de Rango</title>
	<mediaobject>
	  <imageobject>
	    <imagedata fileref="figures/rangewidgets.png" format="png" align="center"/>
	  </imageobject>
	</mediaobject>
      </figure>

      <para>El c�digo fuente de <ulink
url="examples/rangewidgets.py"><command>rangewidgets.py</command></ulink>
es:</para>

      <programlisting>
    1	#!/usr/bin/env python
    2	
    3	# ejemplo rangewidgets.py
    4	
    5	import pygtk
    6	pygtk.require('2.0')
    7	import gtk
    8	
    9	# Funciones auxiliares
   10	
   11	def make_menu_item(name, callback, data=None):
   12	    item = gtk.MenuItem(name)
   13	    item.connect("activate", callback, data)
   14	    item.show()
   15	    return item
   16	
   17	def scale_set_default_values(scale):
   18	    scale.set_update_policy(gtk.UPDATE_CONTINUOUS)
   19	    scale.set_digits(1)
   20	    scale.set_value_pos(gtk.POS_TOP)
   21	    scale.set_draw_value(gtk.TRUE)
   22	
   23	class RangeWidgets:
   24	    def cb_pos_menu_select(self, item, pos):
   25	        # Fijar la posici�n del valor para ambos ajustes
   26	        self.hscale.set_value_pos(pos)
   27	        self.vscale.set_value_pos(pos)
   28	
   29	    def cb_update_menu_select(self, item, policy):
   30	        # Establecer la pol�tica de actualizaci�n para ambos controles
   31	        self.hscale.set_update_policy(policy)
   32	        self.vscale.set_update_policy(policy)
   33	
   34	    def cb_digits_scale(self, adj):
   35	        # Fijar el n�mero de posiciones decimales a las que se ajusta el valor
   36	        self.hscale.set_digits(adj.value)
   37	        self.vscale.set_digits(adj.value)
   38	
   39	    def cb_page_size(self, get, set):
   40	        # Fijar el valor del tama�o de p�gina y de su incremento
   41	        # para el ajuste al valor especificado por la escala "Page Size"
   42	        set.page_size = get.value
   43	        set.page_incr = get.value
   44	        # Ahora emitir la se�al "changed" para reconfigurar todos los controles
   45	        # que est�n ligados a este ajuste
   46	        set.emit("changed")
   47	
   48	    def cb_draw_value(self, button):
   49	        # Activar o desactivar la representaci�n del valor en funci�n del
   50	        # estado del bot�n de activaci�n
   51	        self.hscale.set_draw_value(button.get_active())
   52	        self.vscale.set_draw_value(button.get_active())  
   53	
   54	    # crea la ventana de ejemplo
   55	
   56	    def __init__(self):
   57	        # Creaci�n de la ventana principal
   58	        self.window = gtk.Window (gtk.WINDOW_TOPLEVEL)
   59	        self.window.connect("destroy", lambda w: gtk.main_quit())
   60	        self.window.set_title("range controls")
   61	
   62	        box1 = gtk.VBox(gtk.FALSE, 0)
   63	        self.window.add(box1)
   64	        box1.show()
   65	
   66	        box2 = gtk.HBox(gtk.FALSE, 10)
   67	        box2.set_border_width(10)
   68	        box1.pack_start(box2, gtk.TRUE, gtk.TRUE, 0)
   69	        box2.show()
   70	
   71	        # value, lower, upper, step_increment, page_increment, page_size
   72	        # Obs�rvese que el valor page_size solamente es significativo
   73	        # para controles de barra de desplazamiento y que el valor m�s alto posible es
   74	        # (upper - page_size).
   75	        adj1 = gtk.Adjustment(0.0, 0.0, 101.0, 0.1, 1.0, 1.0)
   76	  
   77	        self.vscale = gtk.VScale(adj1)
   78	        scale_set_default_values(self.vscale)
   79	        box2.pack_start(self.vscale, gtk.TRUE, gtk.TRUE, 0)
   80	        self.vscale.show()
   81	
   82	        box3 = gtk.VBox(gtk.FALSE, 10)
   83	        box2.pack_start(box3, gtk.TRUE, gtk.TRUE, 0)
   84	        box3.show()
   85	
   86	        # Reutilizamos el mismo ajuste
   87	        self.hscale = gtk.HScale(adj1)
   88	        self.hscale.set_size_request(200, 30)
   89	        scale_set_default_values(self.hscale)
   90	        box3.pack_start(self.hscale, gtk.TRUE, gtk.TRUE, 0)
   91	        self.hscale.show()
   92	
   93	        # Reutilizamos el mismo ajuste otra vez
   94	        scrollbar = gtk.HScrollbar(adj1)
   95	        # Obs�rvese como esto hace que las escalas se actualicen
   96	        # continuamente cuando se mueve la barra de desplazamiento
   97	        scrollbar.set_update_policy(gtk.UPDATE_CONTINUOUS)
   98	        box3.pack_start(scrollbar, gtk.TRUE, gtk.TRUE, 0)
   99	        scrollbar.show()
  100	
  101	        box2 = gtk.HBox(gtk.FALSE, 10)
  102	        box2.set_border_width(10)
  103	        box1.pack_start(box2, gtk.TRUE, gtk.TRUE, 0)
  104	        box2.show()
  105	
  106	        # Un bot�n de activaci�n para definir se se muestra el valor o no
  107	        button = gtk.CheckButton("Display value on scale widgets")
  108	        button.set_active(gtk.TRUE)
  109	        button.connect("toggled", self.cb_draw_value)
  110	        box2.pack_start(button, gtk.TRUE, gtk.TRUE, 0)
  111	        button.show()
  112	  
  113	        box2 = gtk.HBox(gtk.FALSE, 10)
  114	        box2.set_border_width(10)
  115	
  116	        # Un men� de opciones para cambiar la posici�n del valor
  117	        label = gtk.Label("Scale Value Position:")
  118	        box2.pack_start(label, gtk.FALSE, gtk.FALSE, 0)
  119	        label.show()
  120	  
  121	        opt = gtk.OptionMenu()
  122	        menu = gtk.Menu()
  123	
  124	        item = make_menu_item ("Top", self.cb_pos_menu_select, gtk.POS_TOP)
  125	        menu.append(item)
  126	  
  127	        item = make_menu_item ("Bottom", self.cb_pos_menu_select,
  128	                               gtk.POS_BOTTOM)
  129	        menu.append(item)
  130	  
  131	        item = make_menu_item ("Left", self.cb_pos_menu_select, gtk.POS_LEFT)
  132	        menu.append(item)
  133	  
  134	        item = make_menu_item ("Right", self.cb_pos_menu_select, gtk.POS_RIGHT)
  135	        menu.append(item)
  136	  
  137	        opt.set_menu(menu)
  138	        box2.pack_start(opt, gtk.TRUE, gtk.TRUE, 0)
  139	        opt.show()
  140	
  141	        box1.pack_start(box2, gtk.TRUE, gtk.TRUE, 0)
  142	        box2.show()
  143	
  144	        box2 = gtk.HBox(gtk.FALSE, 10)
  145	        box2.set_border_width(10)
  146	
  147	        # Otro men� de opciones m�s, esta vez para la pol�tica de actualizaci�n
  148	        # de los controles de escala
  149	        label = gtk.Label("Scale Update Policy:")
  150	        box2.pack_start(label, gtk.FALSE, gtk.FALSE, 0)
  151	        label.show()
  152	  
  153	        opt = gtk.OptionMenu()
  154	        menu = gtk.Menu()
  155	  
  156	        item = make_menu_item("Continuous", self.cb_update_menu_select,
  157	                              gtk.UPDATE_CONTINUOUS)
  158	        menu.append(item)
  159	  
  160	        item = make_menu_item ("Discontinuous", self.cb_update_menu_select,
  161	                               gtk.UPDATE_DISCONTINUOUS)
  162	        menu.append(item)
  163	  
  164	        item = make_menu_item ("Delayed", self.cb_update_menu_select,
  165	                               gtk.UPDATE_DELAYED)
  166	        menu.append(item)
  167	  
  168	        opt.set_menu(menu)
  169	        box2.pack_start(opt, gtk.TRUE, gtk.TRUE, 0)
  170	        opt.show()
  171	  
  172	        box1.pack_start(box2, gtk.TRUE, gtk.TRUE, 0)
  173	        box2.show()
  174	
  175	        box2 = gtk.HBox(gtk.FALSE, 10)
  176	        box2.set_border_width(10)
  177	  
  178	        # Un control HScale para ajustar el n�mero de d�gitos en las escalas
  179	        # de ejemplo
  180	        label = gtk.Label("Scale Digits:")
  181	        box2.pack_start(label, gtk.FALSE, gtk.FALSE, 0)
  182	        label.show()
  183	
  184	        adj2 = gtk.Adjustment(1.0, 0.0, 5.0, 1.0, 1.0, 0.0)
  185	        adj2.connect("value_changed", self.cb_digits_scale)
  186	        scale = gtk.HScale(adj2)
  187	        scale.set_digits(0)
  188	        box2.pack_start(scale, gtk.TRUE, gtk.TRUE, 0)
  189	        scale.show()
  190	
  191	        box1.pack_start(box2, gtk.TRUE, gtk.TRUE, 0)
  192	        box2.show()
  193	  
  194	        box2 = gtk.HBox(gtk.FALSE, 10)
  195	        box2.set_border_width(10)
  196	  
  197	        # Y un �ltimo control HScale para ajustar el tama�o de p�gina
  198	        # de la barra de desplazamiento.
  199	        label = gtk.Label("Scrollbar Page Size:")
  200	        box2.pack_start(label, gtk.FALSE, gtk.FALSE, 0)
  201	        label.show()
  202	
  203	        adj2 = gtk.Adjustment(1.0, 1.0, 101.0, 1.0, 1.0, 0.0)
  204	        adj2.connect("value_changed", self.cb_page_size, adj1)
  205	        scale = gtk.HScale(adj2)
  206	        scale.set_digits(0)
  207	        box2.pack_start(scale, gtk.TRUE, gtk.TRUE, 0)
  208	        scale.show()
  209	
  210	        box1.pack_start(box2, gtk.TRUE, gtk.TRUE, 0)
  211	        box2.show()
  212	
  213	        separator = gtk.HSeparator()
  214	        box1.pack_start(separator, gtk.FALSE, gtk.TRUE, 0)
  215	        separator.show()
  216	
  217	        box2 = gtk.VBox(gtk.FALSE, 10)
  218	        box2.set_border_width(10)
  219	        box1.pack_start(box2, gtk.FALSE, gtk.TRUE, 0)
  220	        box2.show()
  221	
  222	        button = gtk.Button("Quit")
  223	        button.connect("clicked", lambda w: gtk.main_quit())
  224	        box2.pack_start(button, gtk.TRUE, gtk.TRUE, 0)
  225	        button.set_flags(gtk.CAN_DEFAULT)
  226	        button.grab_default()
  227	        button.show()
  228	        self.window.show()
  229	
  230	def main():
  231	    gtk.main()
  232	    return 0            
  233	
  234	if __name__ == "__main__":
  235	    RangeWidgets()
  236	    main()
</programlisting>

      <para>Se debe advertir que el programa no llama al m�todo
<methodname>connect</methodname>() para el evento "delete_event", sino
solamente a la se�al "destroy". Esto seguir� realizando la acci�n deseada, puesto que
un evento "delete_event" sin tratar resultar� en una se�al "destroy" enviada a la
ventana.</para>
    </sect1>
  </chapter>
