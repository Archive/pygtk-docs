<?xml version="1.0" encoding="iso-8859-1" standalone="no"?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
"http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">

<!-- ********************************************************************** -->
  <chapter id="ch-TimeoutsIOAndIdleFunctions">
    <title>Temporizadores, Entrada/Salida y Funciones de Inactividad</title>

<!-- ===================================================================== -->
    <sect1 id="sec-Timeouts">
      <title>Temporizadores</title>

      <para>Puede que estes pregunt�ndote c�mo hacer que GTK haga algo �til
mientras est� dentro de la funci�n <function>main</function>(). Bien, tienes
varias opciones. Usando la siguiente funci�n puedes crear un temporizador que
ser� llamado en intervalos regulares (en milisegundos).</para>

      <programlisting>
  source_id = gobject.timeout_add(<parameter>interval</parameter>, <parameter>function</parameter>, ...)
      </programlisting>

    <para>El argumento <parameter>interval</parameter> es el n�mero de milisegundos
entre llamadas sucesivas a tu funci�n. El argumento <parameter>function</parameter>
es la operaci�n que deseas que se llame. Cualquier argumento tras el
segundo se pasar� a tu funci�n como datos. El valor de retorno <parameter>source_id</parameter> es
un entero, que se puede utilizar para eliminar el temporizador llamando
a:</para>

      <programlisting>
  gobject.source_remove(<parameter>source_id</parameter>)
      </programlisting>

      <para>Tambi�n se puede parar el temporizador devolviendo cero o FALSE
(falso) desde tu funci�n. Obviamente esto significa que si quieres que el
temporizador se siga llamando, debes devolver un valor distinto de cero,
como TRUE (verdadero).</para>

      <para>Tu funci�n ser� algo parecido a:</para>

      <programlisting>
  def timeout_callback(...):
      </programlisting>

      <para>El n�mero de argumentos a tu funci�n debe coincidir con el n�mero
de argumentos de datos especificados en <function>timeout_add</function>().
</para>
    </sect1>

<!-- ===================================================================== -->
    <sect1 id="sec-MonitoringIO">
      <title>Monitorizar la Entrada/Salida</title>

      <para>Puedes comprobar si hay algo para leer o escribir a un fichero
(bien a un fichero Python o a un fichero de m�s bajo nivel del Sistema
Operativo) y autom�ticamente invocar una funci�n. �sto es especialmente �til
para aplicaciones de red. La funci�n:</para>

      <programlisting>
  source_id = gobject.io_add_watch(<parameter>source</parameter>, <parameter>condition</parameter>, <parameter>callback</parameter>)
      </programlisting>

      <para>donde el primer argumento (<parameter>source</parameter>) es el fichero
abierto (objeto de fichero de Python o descriptor de fichero de m�s bajo nivel)
que quieres monitorizar. La funci�n <function>gobject.io_add_watch</function>()
usa internamente el descriptor de fichero de bajo nivel, pero la funci�n lo
podr� extraer usando el m�todo <methodname>fileno</methodname>() cuando sea
necesario. El segundo argumento (<parameter>condition</parameter>)
especifica qu� es lo que se quiere monitorizar. Puede ser cualquiera de:</para>

      <programlisting>
  gobject.IO_IN - Llama a tu funci�n cuando en el fichero hay datos disponibles para leer.

  gobject.IO_OUT - Llama a tu funci�n cuando el fichero est� listo para escritura.

  gobject.IO_PRI - Llama a tu funci�n cuando el fichero tiene datos urgentes para leer.

  gobject.IO_ERR - Llama a tu funci�n cuando se da una condici�n de error.

  gobject.IO_HUP - Llama a tu funci�n cuando se ha producido un "cuelgue" (se ha roto la conexi�n, de uso para pipes y sockets generalmente).
      </programlisting>

      <para>Estas constantes se definen en el m�dulo gobject module. Supongo
que ya te has dado cuenta que el tercer argumento,
<parameter>callback</parameter>, es la funci�n que quieres que se llame cuando
las condiciones anteriores se cumplen.</para>

      <para>El valor de retorno, <parameter>source_id</parameter> se puede
usar para parar de monitorizar el fichero usando la siguiente funci�n:</para>

      <programlisting>
  gobject.source_remove(<parameter>source_id</parameter>)
      </programlisting>

      <para>Tu funci�n ser� algo parecido a:</para>

      <programlisting>
  def input_callback(source, condition):
      </programlisting>

      <para>donde <parameter>source</parameter> y <parameter>condition</parameter> son
los que especificaste antes. El valor de source ser� el descriptor de fichero
de bajo nivel y no el objeto fichero Python (es decir, el valor que devuelve
el m�todo de fichero de Python <function>fileno</function>()).</para>

      <para>Tambi�n se puede parar la monitorizaci�n devolviendo cero o FALSE
(falso) desde tu funci�n. Obviamente esto significa que si quieres que el
monitorizaci�n se siga llamando, debes devolver un valor distinto de cero,
como TRUE (verdadero).</para>
    </sect1>

<!-- ===================================================================== -->
    <sect1 id="sec-IdleFunctions">
      <title>Funciones de Inactividad</title>

      <para>�Qu� pasa si quieres que se llame a una funci�n cuando no est�
pasando nada? Usa la funci�n:</para>

      <programlisting>
  source_id = gobject.idle_add(callback, ...)
    </programlisting>

      <para>Cualquier argumento tras el primero (indicados con ...) se
pasan a la funci�n <parameter>callback</parameter> en orden. El valor de retorno
<parameter>source_id</parameter> se utiliza como una referencia al manejador.
</para>

      <para>Esta funci�n hace que GTK llame a la funci�n especificada
cuando no est� pasando nada m�s.</para>

      <para>Y la funci�n ha ser llamada debe ser parecida a:</para>

      <programlisting>
  def callback(...):
      </programlisting>

      <para>donde los argumentos pasados a <function>callback</function>
son los mismos especificados en la funci�n
<function>gobject.idle_add()</function>. Al igual que en otras funciones,
devolviendo FALSE (falso) dejar� de ser llamada de nuevo, y devolviendo TRUE
(verdadero) se la seguir� llamando la pr�xima ocasi�n que haya un tiempo de
inactividad.</para>

      <para>Se puede eliminar una funci�n de inactividad de la cola llamando
la funci�n siguiente:</para>

      <programlisting>
  gobject.source_remove(<parameter>source_id</parameter>)
     </programlisting>

      <para>siendo <parameter>source_id</parameter> el valor devuelto por la
funci�n <function>gobject.idle_add</function>().</para>
    </sect1>
  </chapter>
