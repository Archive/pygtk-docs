<?xml version="1.0" standalone="no"?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
    "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">

<!-- ********************************************************************** -->
  <chapter id="ch-Introduction">
    <title>Introduzione</title>

  <para>PyGTK 2.0 � un set di moduli Python che fornisce a Python un'interfaccia per le GTK+ 2.X.
All'interno di questo documento, PyGTK si riferisce alla versione 2.X di PyGTK, mentre GTK e GTK+
si riferiscono alla versione 2.X di GTK+.  
Il sito principale per PyGTK � <ulink url="http://www.pygtk.org">www.pygtk.org</ulink>.
Lo sviluppatore principale di PyGTK �:</para>

    <itemizedlist>
      <listitem>
	<simpara>
James Henstridge <ulink url="mailto:james@daa.com.au">james@daa.com.au</ulink>
</simpara>
      </listitem>
    </itemizedlist>

  <para>che � assistito dagli sviluppatori elencati nel file AUTHORS nel pacchetto PyGTK e
dalla community PyGTK.
  </para>

    <para>Python � un linguaggio di programmazione interpretato, molto estensibile,
orientato alla programmazione ad oggetti, ed � fornito con un ricco set di moduli
che forniscono accesso ad un'ampia gamma di servizi relativi al sistema operativo, servizi internet
(come HTML, XML, FTP, etc.), grafica (incluso OpenGL, TK, etc.), funzioni per la gestione
delle stringhe, servizi mail (IMAP, SMTP, POP3, etc.), multimedia (audio, JPEG) e servizi
di crittografia. Inoltre ci sono molti altri moduli, messi a disposizione da terzi, capaci di fornire 
molti altri servizi. La licenza di Python � simile alla LGPL, ed � disponibile per
i sistemi operativi Linux, Unix, Windows e Macintosh. Ulteriori informazioni riguardo Python
sono disponibili su www.python.org. Lo sviluppatore principale di Python �:</para>

    <itemizedlist>
      <listitem>
	<simpara>
Guido van Rossum <ulink url="mailto:guido@python.org">guido@python.org</ulink>
</simpara>
      </listitem>
    </itemizedlist>

    <para>GTK (GIMP Toolkit) � una libreria per sviluppare interfacce grafiche. 
E' rilasciata con licenza LGPL, perci� � possibile sviluppare software open source, software libero,
o anche software commerciale e non libero basato su GTK senza dover pagare niente in licenze
o royalties.</para>

    <para>E' chiamato GIMP toolkit in quanto in origine venne scritto per
sviluppare GNU Image Manipulation Program (GIMP), ma adesso GTK � utilizzato
in un ampio numero di progetti, incluso il progetto GNU Network Object Model Environment (GNOME).
GTK � costruito sopra GDK (GIMP Drawing Kit) che � fondamentalmente un wrapper attorno
alle funzioni a basso livello per l'accesso alle funzioni del gestore di finestre (Xlib nel caso
nel caso dell'X Windows System). I principali autori di GTK sono:</para>

    <itemizedlist>
      <listitem>
	<simpara>
Peter Mattis <ulink
url="mailto:petm@xcf.berkeley.edu">petm@xcf.berkeley.edu</ulink>
</simpara>
      </listitem>

      <listitem>
	<simpara>
      Spencer Kimball <ulink
url="mailto:spencer@xcf.berkeley.edu">spencer@xcf.berkeley.edu</ulink>
</simpara>
      </listitem>

      <listitem>
	<simpara>
      Josh MacDonald <ulink
url="mailto:jmacd@xcf.berkeley.edu">jmacd@xcf.berkeley.edu</ulink>
</simpara>
      </listitem>
    </itemizedlist>

    <para>GTK � mantenuto da:</para>

    <itemizedlist>
      <listitem>
	<simpara>
Owen Taylor <ulink url="mailto:otaylor@redhat.com">otaylor@redhat.com</ulink>
</simpara>
      </listitem>

      <listitem>
	<simpara>
Tim Janik <ulink url="mailto:timj@gtk.org">timj@gtk.org</ulink>
</simpara>
      </listitem>
    </itemizedlist>

    <para>GTK essenzialmente � un API orientata ad oggetti. Nonostante sia scritta
completamente in C, � implementata utilizzando l'idea delle classi e delle funzioni 
callback (puntatori a funzione).</para>

    <para>C'� anche un terzo componente, chiamato GLib, che contiene
delle sostituzioni ad alcune chiamate standard, oltre ad alcune funzioni
aggiuntive per gestire liste collegate, etc. Le funzioni sostitutive sono
utilizzate per aumentare la portabilit� di GTK, poich� alcune funzioni
native non sono disponibili o non sono standard in altri unix come ad
esempio la funzione <function>g_strerror</function>(). Altre invece
contengono miglioramenti alle versioni presenti in libc, come ad esempio
<function>g_malloc</function> che possiede delle utilit� per il debug migliori.</para>

    <para>GLib nella versione 2.0 ha incorporato il sistema di tipizzazione che forma 
le fondamenta della gerarchia delle classi di GTK, il sistema di segnali che � usato in 
tutte le GTK, una API per i thread che astrae le differenti API native per i thread delle 
varie piattaforme ed un automatismo per caricare i moduli.</para>

    <para>Come ultimo componente, GTK utilizza la libreria Pango per 
l'internazionalizzazione del testo di output.</para>

    <para>Questo tutorial descrive l'interfaccia Python per GTK+ ed � basato su
GTK+ 2.0 Tutorial scritto da Tony Gale e Ian Main. Questo tutorial cerca di documentare
il pi� possibile PyGTK, ma non � assolutamente da considerarsi come una guida completa.</para>

    <para>Questo tutorial prevede che il lettore abbia qualche nozione di Python, e sappia
creare ed eseguire programmi scritti in Python. Se non avete familiarit� con Python, vi consiglio di
leggere il <ulink url="http://www.python.org/doc/current/tut/tut.html">Python
Tutorial</ulink> prima di procedere alla lettura del presente documento. 
Questo tutoria non prevede che voi conosciate GTK; se state imparando PyGTK per imparare GTK,
siete pregati di segnarlarci come avete trovato questo tutoria, e se avete problemi. Questo
tutorial non spiega come compilare o installare Python, GTK+ o PyGTK.</para>

<para>Questo tutorial prende spunto da:</para>

    <itemizedlist>
      <listitem><simpara>GTK+ 2.0 through GTK+ 2.4</simpara></listitem>
      <listitem><simpara>Python 2.2</simpara></listitem>
      <listitem><simpara>PyGTK 2.0 through PyGTK 2.4</simpara></listitem>
</itemizedlist>

<para>Gli esempi sono stati scritti e testati su un sistema con RedHat 9.0.</para>

<para>Questo documento � "work in progress". Controllate se ci sono aggiornamenti su
<ulink
url="http://www.pygtk.org/pygtktutorial">www.pygtk.org</ulink>.</para>

<para>Mi piacerebbe molto sapere i problemi che incontrate nell'apprendere PyGTK
da questo documento, e apprezzo suggerimenti su come migliorarlo.
Per maggiori informazioni controllate la sezione Contributing.
Se trovate dei bug segnalateli a <ulink url="http://bugzilla.gnome.org">bugzilla.gnome.org</ulink> 
nella sezione "pygtk project". Le informazioni su <ulink url="http://www.pygtk.org/feedback.html">www.pygtk.org</ulink>
riguardo l'utilizzo di Bugzilla possono esservi d'aiuto.</para>

  <para>PyGTK 2.0 Reference Manual � disponibile all'indirizzo:
<ulink url="http://www.pygtk.org/pygtkreference">http://www.pygtk.org/pygtkreference</ulink>. 
Il manuale tratta dettagliatamente le classi di PyGTK.</para>

  <para>Il sito di PyGTK (<ulink
url="http://www.pygtk.org">www.pygtk.org</ulink>) contiene altre risorse
utili per conoscere PyGTK, incluso un link alla dettagliata <ulink
url="http://www.async.com.br/faq/pygtk/">FAQ</ulink> e altri articoli e
tutorials, oltre ad una mailing list attiva ed un canale IRC (guardate <ulink
url="http://www.pygtk.org/feedback.html">www.pygtk.org</ulink> per maggiori informazioni).</para>

<!-- ===================================================================== -->
    <sect1 id="sec-ExploringPygtk">
      <title>Esplorando PyGTK</title>

      <para>Johan Dahlin ha scritto un piccolo programma in Python (<ulink
url="examples/pygtkconsole.py"><command>pygtkconsole.py</command></ulink>)
che gira su Linux e che permette un'esplorazione interattiva di PyGTK.
Il programma fornisce un interprete interattivo simile a Python, che comunica con
un processo figlio che esegue i comandi inseriti. I moduli PyGTK vengono caricati
automaticamente. Una semplice sessione di esempio �:</para>

<screen>
<prompt>  moe: 96:1095$</prompt> <command>pygtkconsole.py</command>
  Python 2.2.2, PyGTK 1.99.14 (Gtk+ 2.0.6)
  Interactive console to manipulate GTK+ widgets.
  >>> w=Window()
  >>> b=Button('Hello')
  >>> w.add(b)
  >>> def hello(b):
  ...     print "Hello, World!"
  ... 
  >>> b.connect('clicked', hello)
  5
  >>> w.show_all()
  >>> Hello, World!
  Hello, World!
  Hello, World!
  
  >>> b.set_label("Hi There")
  >>> 
</screen>

      <para>Questo esempio crea una finestra contenente un bottone che
stampa a video un messaggio ('Hello, World!') quando viene cliccato.
Questo programma permette di provare facilmente i vari elementi di GTK
e l'interfaccia PyGTK.</para>

    <para>Utilizzo anche un programma sviluppato da Brian McErlean,
<ulink
url="http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/65109">ActiveState
recipe 65109</ulink> con alcune modifiche per farlo girare con PyGTK 2.X. Lo chiamo
<ulink url="examples/gpython.py"><command>gpython.py</command></ulink>. Funziona
in modo simile a <ulink
url="examples/pygtkconsole.py"><command>pygtkconsole.py</command></ulink>
.</para>

    <note>
      <para>E' noto che entrambi i programmi non funzionano con Microsoft Windows
perch� fanno riferimento ad interfacce specifiche di Unix.</para>
    </note>

    </sect1>

  </chapter>
