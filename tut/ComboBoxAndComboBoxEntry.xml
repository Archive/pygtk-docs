<?xml version="1.0" standalone="no"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
    "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">

<sect1 id="sec-ComboBoxAndComboboxEntry">
  <title>ComboBox and ComboBoxEntry Widgets</title>

  <sect2 id="sec-ComboBox">
    <title>ComboBox Widgets</title>

    <para>The <classname>ComboBox</classname> replaces the
<classname>OptionMenu</classname> with a powerful widget that uses a
<classname>TreeModel</classname> (usually a
<classname>ListStore</classname>) to provide the list items to display. The
<classname>ComboBox</classname> implements the
<classname>CellLayout</classname> interface that provides a number of
methods for managing the display of the list items. One or more
<classname>CellRenderers</classname> can be packed into a
<classname>ComboBox</classname> to customize the list item display.</para>

    <sect3 id="sec-BasicComboBox">
      <title>Basic ComboBox Use</title>

      <para>The easy way to create and populate a
<classname>ComboBox</classname> is to use the convenience function:</para>

      <programlisting>
  combobox = gtk.combo_box_new_text()
</programlisting>

      <para>This function creates a <classname>ComboBox</classname> and its
associated <classname>ListStore</classname> and packs it with a
<classname>CellRendererText</classname>. The following convenience methods
are used to populate or remove the contents of the
<classname>ComboBox</classname> and its
<classname>ListStore</classname>:</para>

      <programlisting>
  combobox.append_text(<parameter role="keyword">text</parameter>)
  combobox.prepend_text(<parameter role="keyword">text</parameter>)
  combobox.insert_text(<parameter role="keyword">position</parameter>, <parameter role="keyword">text</parameter>)
  combobox.remove_text(<parameter role="keyword">position</parameter>)
</programlisting>

      <para>where <parameter>text</parameter> is the string to be added to the
<classname>ComboBox</classname> and <parameter>position</parameter> is the
index where <parameter>text</parameter> is to be inserted or removed. In
most cases the convenience function and methods are all you need.</para>

      <para>The example program <ulink
url="examples/comboboxbasic.py">comboboxbasic.py</ulink> demonstrates the
use of the above function and methods. <xref
          linkend="comboboxbasicfig"></xref> illustrates the program in
operation:</para>

      <figure id="comboboxbasicfig">
        <title>Basic ComboBox</title>
        <mediaobject>
          <imageobject>
            <imagedata fileref="figures/comboboxbasic.png" format="png" align="center"></imagedata>
          </imageobject>
        </mediaobject>
      </figure>

      <para>The active text can be retrieved using the method:</para>

        <programlisting>
  text = combobox.get_active_text()
        </programlisting>

      <para>Prior to version 2.6, the <literal>GTK</literal>+ developers
          did not provide such a convenience method to retrieve the active text,
          so you'd have to create your own implementation, similar to:</para>

      <programlisting>
  def get_active_text(combobox):
      model = combobox.get_model()
      active = combobox.get_active()
      if active &lt; 0:
          return None
      return model[active][0]
</programlisting>


      <para>The index of the active item is retrieved using the method:</para>

      <programlisting>
  active = combobox.get_active()
</programlisting>

      <para>The active item can be set using the method:</para>

      <programlisting>
  combobox.set_active(<parameter role="keyword">index</parameter>)
</programlisting>

      <para>where <parameter>index</parameter> is an integer larger than
-2. If <parameter>index</parameter> is -1 there is no active item and the
ComboBox display will be blank. If <parameter>index</parameter> is less than
-1, the call will be ignored. If <parameter>index</parameter> is greater
than -1 the list item with that index value will be displayed.</para>

      <para>You can connect to the "changed" signal of a
<classname>ComboBox</classname> to be notified when the active item has been
changed. The signature of the "changed" handler is:</para>

      <programlisting>
  def changed_cb(combobox, ...):
</programlisting>

      <para>where <parameter>...</parameter> represents the zero or more
arguments passed to the <methodname>GObject.connect</methodname>()
method.</para>

    </sect3>

    <sect3 id="sec-AdvancedComboBox">
      <title>Advanced ComboBox Use</title>

      <para>Creating a <classname>ComboBox</classname> using the
<function>gtk.combo_box_new_text</function>() function is roughly
equivalent to the following code:</para>

      <programlisting>
  liststore = gtk.ListStore(str)
  combobox = gtk.ComboBox(liststore)
  cell = gtk.CellRendererText()
  combobox.pack_start(cell, True)
  combobox.add_attribute(cell, 'text', 0)  
</programlisting>

      <para>To make use of the power of the various
<classname>TreeModel</classname> and <classname>CellRenderer</classname>
objects you need to construct a <classname>ComboBox</classname> using the
constructor:</para>

      <programlisting>
  combobox = gtk.ComboBox(<parameter role="keyword">model</parameter>=None)
</programlisting>

      <para>where <parameter>model</parameter> is a
<classname>TreeModel</classname>. If you create a
<classname>ComboBox</classname> without associating a
<classname>TreeModel</classname>, you can add one later using the
method:</para>

      <programlisting>
  combobox.set_model(<parameter role="keyword">model</parameter>)
</programlisting>

      <para>The associated <classname>TreeModel</classname> can be retrieved
using the method:</para>

      <programlisting>
  model = combobox.get_model()
</programlisting>

      <para>Some of the things you can do with a
<classname>ComboBox</classname> are:</para>

      <itemizedlist>
        <listitem>
          <simpara>Share the same <classname>TreeModel</classname> with
other <classname>ComboBox</classname>es and
<classname>TreeView</classname>s.</simpara>
        </listitem>
        <listitem>
          <simpara>Display images and text in the
<classname>ComboBox</classname> list items.</simpara>
        </listitem>
        <listitem>
          <simpara>Use an existing <classname>TreeStore</classname> or
<classname>ListStore</classname> as the model for the
<classname>ComboBox</classname> list items.</simpara>
        </listitem>
        <listitem>
          <simpara>Use a <classname>TreeModelSort</classname> to provide a
sorted <classname>ComboBox</classname> list.</simpara>
        </listitem>
        <listitem>
          <simpara>Use a <classname>TreeModelFilter</classname> to use a
subtree of a <classname>TreeStore</classname> as the source for a
<classname>ComboBox</classname> list items.</simpara>
        </listitem>
        <listitem>
          <simpara>Use a <classname>TreeModelFilter</classname> to use a
subset of the rows in a <classname>TreeStore</classname> or
<classname>ListStore</classname> as the <classname>ComboBox</classname> list
items.</simpara>
        </listitem>
        <listitem>
          <simpara>Use a cell data function to modify or synthesize the
display for list items.</simpara>
        </listitem>
      </itemizedlist>

      <para>The use of the <classname>TreeModel</classname> and
<classname>CellRenderer</classname> objects is detailed in <xref
linkend="ch-TreeViewWidget"></xref>.</para>

      <para>The <classname>ComboBox</classname> list items can be displayed
in a grid if you have a large number of items to display. Otherwise the list
will have scroll arrows if the entire list cannot be displayed. The
following method is used to set the number of columns to display:</para>

      <programlisting>
  combobox.set_wrap_width(<parameter role="keyword">width</parameter>)
</programlisting>

      <para>where <parameter>width</parameter> is the number of columns of
the grid displaying the list items. For example, the <ulink
url="examples/comboboxwrap.py">comboboxwrap.py</ulink> program displays a
list of 50 items in 5 columns. <xref linkend="comboboxwrapfig"></xref>
illustrates the program in operation:</para>

      <figure id="comboboxwrapfig">
        <title>ComboBox with Wrapped Layout</title>
        <mediaobject>
          <imageobject>
            <imagedata fileref="figures/comboboxwrap.png" format="png" align="center"></imagedata>
          </imageobject>
        </mediaobject>
      </figure>

      <para>With a large number of items, say more than 50, the use of the
<methodname>set_wrap_width</methodname>() method will have poor performance
because of the computation for the grid layout. To get a feel for the affect
modify the <ulink url="examples/comboboxwrap.py">comboboxwrap.py</ulink>
program line 18 to display 150 items.</para>

      <programlisting>
        for n in range(150):
</programlisting>

      <para>Run the program and get a time estimate for startup. Then modify
 it by commenting out line 17:</para>

      <programlisting>
        #combobox.set_wrap_width(5)
</programlisting>

      <para>Run and time it again. It should start up significantly
faster. My experience is about 20 times faster.</para>

      <para>In addition to the <methodname>get_active</methodname>() method
described above, you can retrieve a <classname>TreeIter</classname> pointing
at the active row by using the method:</para>

      <programlisting>
  iter = combobox.get_active_iter()
</programlisting>

      <para>You can also set the active list item using a
<classname>TreeIter</classname> with the method:</para>

      <programlisting>
  combobox.set_active_iter(<parameter role="keyword">iter</parameter>)
</programlisting>

      <para>The <methodname>set_row_span_column</methodname>() and
<methodname>set_column_span_column</methodname>() methods are supposed to
allow the specification of a <classname>TreeModel</classname> column number
that contains the number of rows or columns that the list item is supposed
to span in a grid layout. Unfortunately, in GTK+ 2.4 these methods are
broken.</para>

      <para>Since the <classname>ComboBox</classname> implements the
<classname>CellLayout</classname> interface which has similar capabilities
as the <classname>TreeViewColumn</classname> (see <xref
linkend="sec-TreeViewColumns"></xref> for more information). Briefly, the
interface provides:</para>

      <programlisting>
  combobox.pack_start(<parameter role="keyword">cell</parameter>, <parameter role="keyword">expand</parameter>=True)
  combobox.pack_end(<parameter role="keyword">cell</parameter>, <parameter role="keyword">expand</parameter>=True)
  combobox.clear()
</programlisting>

      <para>The first two methods pack a <classname>CellRenderer</classname>
into the <classname>ComboBox</classname> and the
<methodname>clear</methodname>() method clears all attributes from all
<classname>CellRenderer</classname>s.</para>

      <para>The following methods:</para>

      <programlisting>
  comboboxentry.add_attribute(<parameter role="keyword">cell</parameter>, <parameter role="keyword">attribute</parameter>, <parameter role="keyword">column</parameter>)

  comboboxentry.set_attributes(<parameter>cell</parameter>, <parameter>...</parameter>)
</programlisting>

      <para>set attributes for the <classname>CellRenderer</classname>
specified by <parameter>cell</parameter>. The
<methodname>add_attribute</methodname>() method takes a string
<parameter>attribute</parameter> name (e.g. 'text') and an integer
<parameter>column</parameter> number of the column in the
<classname>TreeModel</classname> to use to set
<parameter>attribute</parameter>. The additional arguments to the
<methodname>set_attributes</methodname>() method are
<literal>attribute=column</literal> pairs (e.g text=1).</para>

    </sect3>

  </sect2>

  <sect2 id="sec-ComboBoxEntry">
    <title>ComboBoxEntry Widgets</title>

    <para>The <classname>ComboBoxEntry</classname> widget replaces the
<classname>Combo</classname> widget. It is subclassed from the
<classname>ComboBox</classname> widget and contains a child
<classname>Entry</classname> widget that has its contents set by selecting
an item in the dropdown list or by direct text entry either from the
keyboard or by pasting from a <classname>Clipboard</classname> or a
selection.</para>

    <sect3 id="sec-BasicComboBoxEntry">
      <title>Basic ComboBoxEntry Use</title>

      <para>Like the <classname>ComboBox</classname>, the
<classname>ComboBoxEntry</classname> can be created using the convenience
function:</para>

      <programlisting>
  comboboxentry = gtk.combo_box_entry_new_text()
</programlisting>

      <para>The <classname>ComboBoxEntry</classname> should be populated
using the <classname>ComboBox</classname> convenience methods described in
<xref linkend="sec-BasicComboBox"></xref>.</para>

      <para>Since a <classname>ComboBoxEntry</classname> widget is a
<classname>Bin</classname> widget its child <classname>Entry</classname>
widget is available using the "child" attribute or the
<methodname>get_child</methodname>() method:</para>

      <programlisting>
  entry = comboboxentry.child
  entry = comboboxentry.get_child()
</programlisting>

      <para>You can retrieve the <classname>Entry</classname> text using its
<methodname>get_text</methodname>() method.</para>

      <para>Like the <classname>ComboBox</classname>, you can track changes
in the active list item by connecting to the "changed"
signal. Unfortunately, this doesn't help track changes to the text in the
<classname>Entry</classname> child that are direct entry. When a direct
entry is made to the child <classname>Entry</classname> widget the "changed"
signal will be emitted but the index returned by the
<methodname>get_active</methodname>() method will be -1. To track all
changes to the <classname>Entry</classname> text, you'll have to use the
<classname>Entry</classname> "changed" signal. For example:</para>

      <programlisting>
  def changed_cb(entry):
      print entry.get_text()

  comboboxentry.child.connect('changed', changed_cb)
</programlisting>

      <para>will print out the text after every change in the child
<classname>Entry</classname> widget. For example, the <ulink
url="examples/comboboxentrybasic.py">comboboxentrybasic.py</ulink> program
demonstrates the use of the convenience API. <xref
          linkend="comboboxentrybasicfig"></xref> illustrates the program in
operation:</para>

      <figure id="comboboxentrybasicfig">
        <title>Basic ComboBoxEntry</title>
        <mediaobject>
          <imageobject>
            <imagedata fileref="figures/comboboxentrybasic.png" format="png" align="center"></imagedata>
          </imageobject>
        </mediaobject>
      </figure>

      <para>Note that when the <classname>Entry</classname> text is changed
due to the selection of a dropdown list item the "changed" handler is called
twice: once when the text is cleared; and, once when the text is set with
the selected list item text.</para>

    </sect3>

    <sect3 id="sec=AdvancedComboBoxEntry">
      <title>Advanced ComboBoxEntry Use</title>

      <para>The constructor for a ComboBoxEntry is:</para>

      <programlisting>
  comboboxentry = gtk.ComboBoxEntry(<parameter role="keyword">model</parameter>=None, <parameter role="keyword">column</parameter>=-1)
</programlisting>

      <para>where <parameter>model</parameter> is a
<classname>TreeModel</classname> and <parameter>column</parameter> is the
number of the column in <parameter>model</parameter> to use for setting the
list items. If column is not specified the default value is -1 which means
the text column is unset.</para>

      <para>Creating a <classname>ComboBoxEntry</classname> using the
convenience function <function>gtk.combo_box_entry_new_text</function>() is
equivalent to the following:</para>

      <programlisting>
  liststore = gtk.ListStore(str)
  comboboxentry = gtk.ComboBoxEntry(liststore, 0)
</programlisting>

      <para>The <classname>ComboBoxEntry</classname> adds a couple of
methods that are used to set and retrieve the
<classname>TreeModel</classname> column number to use for setting the list
item strings:</para>

      <programlisting>
  comboboxentry.set_text_column(<parameter role="keyword">text_column</parameter>)
  text_column = comboboxentry.get_text_column()
</programlisting>

      <para>The text column can also be retrieved and set using the
"text-column" property. See <xref linkend="sec-AdvancedComboBox"></xref> for
more information on the advanced use of the
<classname>ComboBoxEntry</classname>.</para>

      <note>
        <para>Your application must set the text column for the
<classname>ComboBoxEntry</classname> to set the <classname>Entry</classname>
contents from the dropdown list. The text column can only be set once,
either by using the constructor or by using the
<methodname>set_text_column</methodname>() method.</para>
      </note>

      <para>When a <classname>ComboBoxEntry</classname> is created it is
packed with a new <classname>CellRendererText</classname> which is not
accessible. The 'text' attribute for the
<classname>CellRendererText</classname> has to be set as a side effect of
setting the text column using the <methodname>set_text_column</methodname>()
method. You can pack additional <classname>CellRenderer</classname>s into a
<classname>ComboBoxEntry</classname> for display in the dropdown list. See
<xref linkend="sec-AdvancedComboBox"></xref> for more information.</para>

    </sect3>

  </sect2>

</sect1>
