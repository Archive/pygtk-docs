<?xml version="1.0" standalone="no"?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
    "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">

<chapter id="ch-NewInPyGTK2.4"
  xmlns:xi="http://www.w3.org/2001/XInclude">
    <title>New Widgets in PyGTK 2.4</title>

    <para>Quite a few new widgets and support objects were added in PyGTK
2.4 including:</para>

  <itemizedlist>
    <listitem>
      <simpara><classname>Action</classname>,
<classname>RadioAction</classname>, <classname>ToggleAction</classname> -
objects that represent actions that a user can take. Actions contain
information to be used to create proxy widgets ( for example, icons, menu
items and toolbar items).</simpara>
    </listitem>
    <listitem>
      <simpara><classname>ActionGroup</classname> - an object containing
Actions that have some relationship, for example, actions to open, close and
print a document.</simpara>
    </listitem>
    <listitem>
      <simpara><classname>Border</classname> - an object containing the
values for a border.</simpara>
    </listitem>
    <listitem>
      <simpara><classname>ColorButton</classname> - a button used to
launch a ColorSelectionDialog.</simpara>
    </listitem>
    <listitem>
      <simpara><classname>ComboBox</classname> - a widget providing a list
of items to choose from. It replaces the
<classname>OptionMenu</classname>.</simpara>
    </listitem>
    <listitem>
      <simpara><classname>ComboBoxEntry</classname> - a widget providing a
text entry field with a dropdown list of items to choose from. It replaces
the <classname>Combo</classname>.</simpara>
    </listitem>
    <listitem>
      <simpara><classname>EntryCompletion</classname> - an object
providing completion for an <classname>Entry</classname> widget.</simpara>
    </listitem>
    <listitem>
      <simpara><classname>Expander</classname> - a container that can show
and hide its child in response to its button click.</simpara>
    </listitem>
    <listitem>
      <simpara><classname>FileChooser</classname> - an interface for
        choosing files.</simpara>
    </listitem>
    <listitem>
      <simpara><classname>FileChooserWidget</classname> - a widget
implementing the <classname>FileChooser</classname> interface. It replaces
the <classname>FileSelection</classname> widget.</simpara>
    </listitem>
    <listitem>
      <simpara><classname>FileChooserDialog</classname> - a dialog used
for "File/Open" and "File/Save" actions. It replaces the
<classname>FileSelectionDialog</classname>.</simpara>
    </listitem>
    <listitem>
      <simpara><classname>FileFilter</classname> - an object used to
filter files based on an internal set of rules.</simpara>
    </listitem>
    <listitem>
      <simpara><classname>FontButton</classname> - a button that launches
the <classname>FontSelectionDialog</classname>.</simpara>
    </listitem>
    <listitem>
      <simpara><classname>IconInfo</classname> - an object containing
information about an icon in an <classname>IconTheme</classname>.</simpara>
    </listitem>
    <listitem>
      <simpara><classname>IconTheme</classname> - an object providing
lookup of icons by name and size.</simpara>
    </listitem>
    <listitem>
      <simpara><classname>ToolItem</classname>,
<classname>ToolButton</classname>, <classname>RadioToolButton</classname>,
<classname>SeparatorToolItem</classname>,
<classname>ToggleToolButton</classname> - widgets that can be added to a
<classname>Toolbar</classname>. These replace the previous
<classname>Toolbar</classname> items.</simpara>
    </listitem>
    <listitem>
      <simpara><classname>TreeModelFilter</classname> - an object providing
a powerful mechanism for revising the representation of an underlying
<classname>TreeModel</classname>. This is described in <xref
linkend="sec-TreeModelFilter"></xref>.</simpara>
    </listitem>
    <listitem>
      <simpara><classname>UIManager</classname> - an object providing a
way to construct menus and toolbars from an XML UI description. It also has
methods to manage the merging and separation of multiple UI
descriptions.</simpara>
    </listitem>
  </itemizedlist>

  <xi:include href="ActionsAndActionGroups.xml"></xi:include>
  <xi:include href="ComboBoxAndComboBoxEntry.xml"></xi:include>
  <xi:include href="ColorButtonAndFontButton.xml"></xi:include>
  <xi:include href="EntryCompletion.xml"></xi:include>
  <xi:include href="ExpanderWidget.xml"></xi:include>
  <xi:include href="FileChooser.xml"></xi:include>
  <xi:include href="UIManager.xml"></xi:include>

</chapter>
